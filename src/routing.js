import React from 'react';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import { loggedIn, clear } from 'auth';
import { addHttpStatusHandler } from 'utils/ajax';
import MainLayout from 'smart/main-layout';
import Login from 'smart/login';

import Categories from 'smart/categories';
import Entities from 'smart/entities';
import Place from 'smart/place';
import CheckList from 'smart/check-list';
import Task from 'smart/task';
import FAQ from 'smart/FAQ';
import FoodservicePlace from 'smart/foodservice-place';
import Foodservice from 'smart/foodservice';
import CampusBuilding from 'smart/campus-building';

import * as Nav from 'nav';

addHttpStatusHandler(status => {
    if (status === 401) {
        clear();
        hashHistory.replace(Nav.login());
    }
});

function checkAndRedirectToLogin(nextState, replace) {
    if (!loggedIn()) {
        replace({
            pathname: Nav.login(),
            state: { nextPathname: nextState.location.pathname }
        });
    }
}

function checkAndRedirectToIndex(nextState, replace) {
    if (loggedIn()) {
        replace(Nav.index());
    }
}

const Routing = (
    <Router history={hashHistory}>
        <Route path={Nav.index()} component={MainLayout} onEnter={checkAndRedirectToLogin}>
            <IndexRoute component={Categories} />
            <Route path={Nav.entities(':categoryId')} component={Entities} />
            <Route path={Nav.entities(':categoryId', ':subCategoryId')} component={Entities} />

            <Route path={Nav.newPlace(':categoryId')} component={Place} />
            <Route path={Nav.newPlace(':categoryId', ':subCategoryId')} component={Place} />
            <Route path={Nav.place(':placeId', ':categoryId')} component={Place} />
            <Route path={Nav.place(':placeId', ':categoryId', ':subCategoryId')} component={Place} />

            <Route path={Nav.newCheckList(':categoryId')} component={CheckList} />
            <Route path={Nav.checkList(':checkListId', ':categoryId')} component={CheckList} />
            <Router path={Nav.newTask(':checkListId', ':categoryId')} component={Task} />
            <Router path={Nav.task(':taskIndex', ':checkListId', ':categoryId')} component={Task} />

            <Route path={Nav.newFoodservicePlace(':categoryId')} component={FoodservicePlace} />
            <Route path={Nav.foodservicePlace(':foodservicePlaceId', ':categoryId')} component={FoodservicePlace} />
            <Router path={Nav.newFoodservice(':foodservicePlaceId', ':categoryId')} component={Foodservice} />
            <Router path={Nav.foodservice(':foodserviceIndex', ':foodservicePlaceId', ':categoryId')} component={Foodservice} />

            <Route path={Nav.newFAQ(':categoryId')} component={FAQ} />
            <Route path={Nav.FAQ(':faqId', ':categoryId')} component={FAQ} />

            <Route path={Nav.newCampusBuilding(':categoryId')} component={CampusBuilding} />
            <Route path={Nav.campusBuilding(':campusBuildingId', ':categoryId')} component={CampusBuilding} />
        </Route>
        <Route path={Nav.login()} component={Login} onEnter={checkAndRedirectToIndex}/>
    </Router>
);

export default Routing;
