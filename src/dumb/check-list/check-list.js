import React, { PropTypes } from 'react';
import FlatButton from 'material-ui/lib/flat-button';
import LangInput from 'dumb/lang-input';
import FormGroup from 'dumb/form-group';
import Card from 'dumb/card';
import ColorPicker from 'dumb/color-picker';
import { langTextIsNotEmpty } from 'helpers/validate';
import { getColors } from 'provider';
import EntityList from 'dumb/entity-list';
import Form from 'dumb/form';

const { object, func } = PropTypes;

export default class CheckList extends Form {
    static propTypes = {
        data: object,
        onSave: func,
        onTaskSelect: func,
        onTaskDelete: func,
        onTaskAdd: func
    };

    validate = [
        {
            name: 'Название',
            dataField: 'title',
            stateField: 'titleIsValid',
            validator: langTextIsNotEmpty
        },
        {
            name: 'Описание',
            dataField: 'subTitle',
            stateField: 'subTitleIsValid',
            validator: langTextIsNotEmpty
        }
    ];

    constructor(props) {
        super(props);
        this.state = this.calcState(props.data);
    }

    renderContent(d) {
        return (
            <div>
                <Card>
                    <FormGroup title="Название" expandable={false}>
                        <LangInput
                            required
                            value={d.get('title')}
                            placeholder="Название"
                            direction="row"
                            onChange={val => this.handlePropChange('title', val)}
                        />
                    </FormGroup>
                    <FormGroup title="Oписание">
                        <LangInput
                            value={d.get('subTitle')}
                            placeholder="Описание"
                            multiline={true}
                            onChange={val => this.handlePropChange('subTitle', val)}
                        />
                    </FormGroup>
                </Card>
                <Card>
                    <FormGroup title="Цвета">
                        <ColorPicker
                            color={d.get('color')}
                            onChange={val => this.handleColorChange(val)}
                            colors={getColors()}
                            title="Цвет иконки"
                        />
                    </FormGroup>
                </Card>
                {this.renderTaskList(d.get('tasks'))}
                <FlatButton
                    label="Добавить шаг"
                    onTouchTap={() => this.handleAddTask()}
                />
            </div>
        );
    }

    renderTaskList(tasks) {
        if (!tasks || !tasks.size) {
            return;
        }

        return <Card>
            <EntityList
                data={tasks}
                onSelect={(data, i) => this.handleTaskSelect(i)}
                onDelete={(data, i) => this.handleTaskDelete(i)}
                onMoveEnd={() => {}}
                onMove={data => this.handleTaskMove(data)}
                dataConverter={data => {
                    return {
                        id: data.get('id') || data.get('_cid'),
                        primaryText: data.getIn(['title', 'russian'])
                    }
                }}
            />
        </Card>;
    }

    handleColorChange(value) {
        this.setState({
            data: this.state.data.set('color', value.color)
        });
    }

    handleAddTask() {
        this.props.onTaskAdd();
    }

    handleTaskSelect(data) {
        this.props.onTaskSelect(data);
    }

    handleTaskDelete(data) {
        this.props.onTaskDelete(data);
    }

    handleTaskMove(data) {
        this.props.onTaskMove(data);
    }
}
