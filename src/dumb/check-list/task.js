import React from 'react';
import LangInput from 'dumb/lang-input';
import AdditionalInfo from 'dumb/additional-info';
import FormGroup from 'dumb/form-group';
import Card from 'dumb/card';
import { langTextIsNotEmpty } from 'helpers/validate';
import Form from 'dumb/form';

export default class Task extends Form {
    validate = [
        {
            name: 'Название',
            dataField: 'title',
            stateField: 'titleIsValid',
            validator: langTextIsNotEmpty
        },
        {
            name: 'Описание',
            dataField: 'description',
            stateField: 'descriptionIsValid',
            validator: langTextIsNotEmpty
        }
    ];

    constructor(props) {
        super(props);
        this.state = this.calcState(props.data);
    }

    renderContent(d) {
        return (
            <div>
                <Card>
                    <FormGroup title="Название описание" expanded>
                        <LangInput
                            required
                            value={d.get('title')}
                            placeholder="Название"
                            direction="row"
                            onChange={val => this.handlePropChange('title', val)}
                        />
                    </FormGroup>
                    <FormGroup title="Описание" expanded>
                        <LangInput
                            required
                            value={d.get('description')}
                            placeholder="Описание"
                            multiline={true}
                            onChange={val => this.handlePropChange('description', val)}
                        />
                    </FormGroup>
                </Card>

                <AdditionalInfo
                    data={{
                        additionalText: d.get('additionalText'),
                        pageLink: d.get('pageLink'),
                        externalLink: d.get('externalLink'),
                        telephone: d.get('telephone'),
                        contacts: d.get('contacts')
                    }}
                    onChange={val => this.handleAdditionalInfoChange(val)}
                />
            </div>
        );
    }

    handleAdditionalInfoChange(value) {
        this.setState({
            data: this.state.data.merge(value)
        });
    }
}
