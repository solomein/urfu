import React, { Component, PropTypes } from 'react';
import FlatButton from 'material-ui/lib/flat-button';
import css from './card-action-container.css';

export default class CardActionContainer extends Component {
    render() {
        return <section>
            {this.props.children}
            <div className={css.action}>
                <FlatButton
                    label={this.props.actionLabel}
                    onTouchTap={() => this.handleAction()}
                />
            </div>
        </section>;
    }

    handleAction() {
        this.props.onAction();
    }
}
