import React, { Component, PropTypes } from 'react';
import { isFunction } from 'lodash/lang';
import { ControlLabel } from 'dumb/control-label';
import css from './enhanced-input.css';
import { withMods } from 'utils/cssm';

const mod = withMods(css);
const { func, string, bool } = PropTypes;

export default class EnhancedInput extends Component {
    static propTypes = {
        onChange: func.isRequired,
        onFocus: func,
        onBlur: func,
        onKeyPress: func,
        value: string,
        placeholder: string,
        label: string,
        disabled: bool,
        message: string,
        invalid: bool,
        autofocus: bool,
        type: string
    };

    static defaultProps = {
        placeholder: '',
        invalid: false,
        message: '',
        label: '',
        disabled: false,
        autofocus: false,
        value: '',
        type: 'text'
    };

    constructor(props) {
        super(props);
        this.state = {
            value: this.props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        const { value } = nextProps;
        this.setState({ value });
    }

    render() {
        const className = mod.root({
            invalid: this.props.invalid
        });

        return <div className={className}>
            <ControlLabel>{this.props.label}</ControlLabel>
            <div className={css.controlWrapper}>
                {this.renderControl()}
                <div className={css.bar} />
            </div>
            {this.renderMessage()}
        </div>;
    }

    renderControl() {
        return <input
            className={css.control}
            value={this.state.value}
            placeholder={this.props.placeholder}
            disabled={this.props.disabled}
            onChange={this.handleChange.bind(this)}
            onFocus={this.handleFocus.bind(this)}
            onKeyPress={this.handleKeyPress.bind(this)}
            onBlur={this.props.onBlur}
            autoFocus={this.props.autofocus}
            type={this.props.type}
            ref="control"
        />;
    }

    renderMessage() {
        if (!this.props.message.length) return;
        return <div className={css.message}>{this.props.message}</div>
    }

    handleFocus(ev) {
        if (isFunction(this.props.onFocus)) {
            this.props.onFocus(ev, this.state.value);
        }
    }

    handleKeyPress(ev) {
        if (isFunction(this.props.onKeyPress)) {
            this.props.onKeyPress(ev, this.state.value);
        }
    }

    handleChange(ev) {
        this.setState({ value: ev.currentTarget.value });
        this.props.onChange(ev.currentTarget.value, ev);
    }

    focus() {
        this.refs.control.focus();
    }
}
