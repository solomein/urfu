import React, { Component } from 'react';
import EnhancedInput from './enhanced-input';
import EnhancedTextarea from './enhanced-textarea';

const { func, string, bool } = React.PropTypes;

export default class TextField extends Component {
    static propTypes = {
        onChange: func.isRequired,
        onKeyPress: func,
        value: string,
        multiline: bool,
        placeholder: string,
        label: string,
        disabled: bool,
        onFocus: func,
        onBlur: func,
        invalid: bool,
        message: string,
        autoFocus: bool,
        type: string
    };

    static defaultProps = {
        multiline: false
    };

    shouldComponentUpdate(nextProps) {
        return nextProps.value !== this.props.value;
    }

    render() {
        return (
            <section>
                {this.props.multiline
                    ? React.createElement(EnhancedTextarea, { ...this.props })
                    : React.createElement(EnhancedInput, {...this.props})
                }
            </section>
        );
    }
}
