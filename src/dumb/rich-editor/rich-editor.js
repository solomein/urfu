import React, { Component, PropTypes } from 'react';
import '!!style?singleton!css!vendors/lepture/editor.css';
import marked from 'vendors/lepture/marked';
import { Editor } from 'vendors/lepture/editor';
import './rich-editor.css';

const { func, string } = PropTypes;

Editor.markdown = marked;

export default class RichEditor extends Component {
    static propTypes = {
        onChange: func,
        value: string
    };

    static defaultProps = {
        value: ''
    };

    componentDidMount() {
        this.editor = new Editor({
            element: this.refs.textarea,
            toolbar: [
                {
                    name: 'bold',
                    action: Editor.toggleBold
                },
                {
                    name: 'italic',
                    action: Editor.toggleItalic
                },
                '|',
                {
                    name: 'unordered-list',
                    action: Editor.toggleUnOrderedList
                },
                {
                    name: 'ordered-list',
                    action: Editor.toggleOrderedList
                },
                '|',
                {
                    name: 'preview',
                    action: Editor.togglePreview
                }
            ]
        });

        this.bindEditor();
        this.setValue(this.props.value);
    }

    componentWillUnmount() {
        // delete editor
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value !== this.editor.codemirror.getValue()) {
            this.setValue(nextProps.value);
        }
    }

    render() {
        return <section>
            <textarea ref="textarea" />
        </section>;
    }

    bindEditor() {
        this.editor.codemirror.on('change', this.handleEditorChange);
    }

    unbindEditor() {
        this.editor.codemirror.off('change', this.handleEditorChange);
    }

    handleEditorChange = () => {
        const value = this.editor.codemirror.getValue();
        this.props.onChange(value);
    };

    setValue(value) {
        this.editor.codemirror.setValue(value || '');
    }
}
