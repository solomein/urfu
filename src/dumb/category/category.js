import React, { Component, PropTypes } from 'react';
import { withMods } from 'utils/cssm';
import css from './category.css';

const mod = withMods(css);
const { func, string } = PropTypes;

export default class Category extends Component {
    static propTypes = {
        onSelect: func.isRequired,
        id: string.isRequired,
        name: string.isRequired,
        type: string.isRequired,
        color: string
    };

    render() {
        const style = {};
        if (this.props.color) {
            style.background = this.props.color;
        }

        return <div
            style={style}
            className={mod.root({ type: this.props.type })}
            onClick={this.handleClick.bind(this)}
        >
            <div className={css.iconWrap}>
                <div className={css.icon}></div>
            </div>
            <div className={css.text}>{this.props.name}</div>
        </div>;
    }

    handleClick() {
        this.props.onSelect({
            id: this.props.id,
            type: this.props.type
        });
    }
}
