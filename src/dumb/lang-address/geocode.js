import { getUrl } from 'utils/ajax';
import fetchJsonp from 'fetch-jsonp';

export function parseMetadataProp(prop) {
    try {
        const t = prop
            .GeocoderMetaData
            .AddressDetails.Country.AdministrativeArea
            .SubAdministrativeArea
            .Locality
            .Thoroughfare;

        return {
            thoroughfare: t.ThoroughfareName,
            premise: t.Premise.PremiseNumber
        };
    } catch(ev) {
        return null;
    }
}

function getter(lang, parser) {
    return function (coords) {

        return fetchJsonp(getUrl('https://geocode-maps.yandex.ru/1.x/', {
            geocode: `${coords[1]},${coords[0]}`,
            format: 'json',
            results: 1,
            lang: lang
        }))
        .then(response => {
            return response.json();
        })
        .then(({ response }) => {
            const result = response
                .GeoObjectCollection
                .featureMember[0]
                .GeoObject
                .metaDataProperty;
            const name = parseMetadataProp(result);

            if (!name) {
                return null;
            }

            return parser(name.thoroughfare, name.premise);
        })
    }
}

export const getEnName = getter('en_US', parseEnName);
export const getRuName = getter('ru_RU', parseRuName);

function parseEnName(thoroughfare, premise) {
    return `${premise} ${parseEnThoroughfare(thoroughfare)}`;
}

function parseRuName(thoroughfare, premise) {
    return `${parseRuThoroughfare(thoroughfare)}, ${premise}`;
}

function parseEnThoroughfare(thoroughfare) {
    const ave = /^prospekt | prospekt$/;
    if(ave.test(thoroughfare)) {
        return `${thoroughfare.replace(ave, '')} ave.`;
    }

    const st = /^ulitsa | ulitsa$/;
    if (st.test(thoroughfare)) {
        return `${thoroughfare.replace(st, '')} st.`;
    }

    return thoroughfare;
}

function parseRuThoroughfare(thoroughfare) {
    const ave = /^проспект | проспект$/;
    if(ave.test(thoroughfare)) {
        return `пр. ${thoroughfare.replace(ave, '')}`;
    }

    const st = /^улица | улица$/;
    if (st.test(thoroughfare)) {
        return `ул. ${thoroughfare.replace(st, '')}`;
    }

    return thoroughfare;
}
