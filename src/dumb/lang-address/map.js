/*global ymaps*/
import React, { Component, PropTypes } from 'react';
import css from './lang-address.css';
import { Map } from 'immutable';
import { getEnName, getRuName } from './geocode';
import loadScript from 'utils/load-script';

const { object, func } = PropTypes;
let ymapsScript = null;

function loadYmaps() {
    if (ymapsScript !== null) {
        return ymapsScript;
    }

    return ymapsScript = loadScript('//api-maps.yandex.ru/2.1/?lang=ru_RU');
}

export default class Address extends Component {
    static propTypes = {
        value: object,
        onChange: func.isRequired
    };

    static defaultProps = {
        value: Map({
            latitude: 56.8375,
            longitude: 60.6033
        })
    };

    calcState(props) {
        return {
            value: props.value
        };
    }

    constructor(props) {
        super(props);
        this.state = this.calcState(props);
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.value) {
            return;
        }
        this.setState({ value: nextProps.value });
    }

    componentDidMount() {
        this.initMap();
    }

    componentDidUpdate() {
        this.initMap();
    }

    componentWillUnmount() {
        this.destroyMap();
    }

    render() {
        return <div className={css.map} ref="map"></div>;
    }

    getCoordinates() {
        const coordinates = this.state.value;
        return [
            coordinates.get('latitude'),
            coordinates.get('longitude')
        ];
    }

    initMap() {
        if (this.map) {
            this.map.panTo(this.getCoordinates());
            this.mark.geometry.setCoordinates(this.getCoordinates());
            return;
        }

        loadYmaps().then(() => {
            ymaps.ready(() => {
                this.createMap();
            });
        });
    }

    createMap() {
        const searchControl = new ymaps.control.SearchControl({
            options: {
                suppressYandexSearch: true,
                noPlacemark: true
            }
        });

        searchControl.events.add('resultselect', ev => {
            const result = searchControl.getResultsArray()[ev.get('index')];
            const c = result.geometry.getCoordinates();
            this.handleCoordinatesChange(c, true);
        });

        this.map = new ymaps.Map(this.refs.map, {
            center: this.getCoordinates(),
            zoom: 13,
            controls: ['zoomControl', 'fullscreenControl']
        }, {
            searchControlProvider: 'yandex#map'
        });

        this.map.controls.add(searchControl);

        this.mark = new ymaps.Placemark(this.getCoordinates(), {}, {
            preset: 'islands#icon',
            iconColor: '#e50079',
            draggable: true
        });

        this.map.geoObjects.add(this.mark);

        this.mark.events.add('dragend', () => {
            const c = this.mark.geometry.getCoordinates();
            this.handleCoordinatesChange(c, true);
        });
    }

    parseCoords(coords) {
        return Promise.all([getRuName(coords), getEnName(coords)]);
    }

    destroyMap() {
        if (this.map) {
            try {
                this.map.destroy();
            } catch(err) {
                return;
            }
        }
    }

    handleCoordinatesChange([latitude, longitude], parse) {
        const value = this.state.value.merge({
            latitude,
            longitude
        });

        if (parse) {
            this.parseCoords([latitude, longitude])
                .then(v => this.props.onChange(value, v));
        } else {
            this.props.onChange(value);
        }
    }
}
