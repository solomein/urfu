import React, { Component, PropTypes } from 'react';
import Flag from 'dumb/flag';
import css from './lang-address.css';
import { TextField } from 'dumb/text-field';
import { Map } from 'immutable';
import Ymap from './map';

const { object, func } = PropTypes;

export default class Address extends Component {
    static propTypes = {
        value: object,
        onChange: func
    };

    static defaultProps = {
        value: Map()
    };

    calcState(props) {
        return {
            invalid: Map({
                russian: false,
                english: false
            }),
            value: props.value
        };
    }

    constructor(props) {
        super(props);
        this.state = this.calcState(props);
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.value) {
            return;
        }
        this.setState({ value: nextProps.value });
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.state.value !== nextState.value
            || this.state.invalid !== nextState.invalid;
    }

    render() {
        return <section className={css.root}>
            <Ymap
                value={this.state.value.get('coordinates')}
                onChange={(val, t) => this.handleCoordinatesChange(val, t)}
            />
            <div className={css.textControls}>
                {this.renderTextComponent('ru', 'russian')}
                {this.renderTextComponent('us', 'english')}
            </div>
        </section>;
    }

    renderTextComponent(country, lang) {
        return <div className={css.lang}>
            <Flag country={country} />
            <div className={css.controls}>
                <div className={css.textField}>
                    <TextField
                        value={this.state.value.getIn(['title', lang])}
                        label="Адрес"
                        placeholder="Адрес"
                        onChange={val => this.handleTitleChange(val, lang)}
                        message="Обязательное поле"
                        invalid={this.state.invalid.get(lang)}
                    />
                </div>
                <div className={css.textField}>
                    <TextField
                        value={this.state.value.getIn(['details', lang])}
                        label="Дополнительная информация"
                        placeholder="Дополнительная информация"
                        onChange={val => this.handleDetailsChange(val, lang)}
                    />
                </div>
            </div>
        </div>;
    }

    handleCoordinatesChange(val, titles) {
        let value = this.state.value.set('coordinates', val);

        if (titles) {
            value = value.mergeIn(['title'], {
                russian: titles[0] ? titles[0] : '',
                english: titles[1] ? titles[1] : ''
            });
        }

        this.setState({ value });
        this.props.onChange(value);
    }

    handleTitleChange(val, lang) {
        const value = this.state.value.setIn(['title', lang], val);

        this.setState({
            value,
            invalid: this.state.invalid.set(lang, val.length === 0)
        });
        this.props.onChange(value);
    }

    handleDetailsChange(val, lang) {
        const value = this.state.value.setIn(['details', lang], val);

        this.setState({ value });
        this.props.onChange(value);
    }
}
