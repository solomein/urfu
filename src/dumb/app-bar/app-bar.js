import React, { Component, PropTypes } from 'react';
import ActionExit from 'material-ui/lib/svg-icons/action/exit-to-app';
import NavigationArrowBack from 'material-ui/lib/svg-icons/navigation/arrow-back';
import IconButton from 'material-ui/lib/icon-button';
import css from './app-bar.css';

const { string, func, bool } = PropTypes;

export default class Bar extends Component {
    static propTypes = {
        title: string,
        onLogout: func,
        onBack: func,
        showBack: bool
    };

    shouldComponentUpdate(nextProps) {
        return nextProps.title !== this.props.title;
    }

    render() {
        return <section className={css.root}>
            {this.renderBack()}
            <div className={css.title}>
                {this.props.title}
            </div>
            <IconButton tooltip="Выход" onTouchTap={() => this.handleLogout()}>
                <ActionExit color="#fff" />
            </IconButton>
        </section>;
    }

    renderBack() {
        if (this.props.showBack) {
            return <IconButton onTouchTap={() => this.handleBack()}>
                <NavigationArrowBack color="#fff" />
            </IconButton>
        }
    }

    handleLogout() {
        this.props.onLogout();
    }

    handleBack() {
        this.props.onBack();
    }
}
