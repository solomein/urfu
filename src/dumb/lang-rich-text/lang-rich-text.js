import React, { Component, PropTypes } from 'react';
import css from './lang-rich-text.css';
import Flag from 'dumb/flag';
import RichEditor from 'dumb/rich-editor';
import { Map } from 'immutable';

const { object, func } = PropTypes;

export default class LangRichText extends Component {
    static propTypes = {
        value: object,
        onChange: func
    };

    static defaultProps = {
        value: Map()
    };

    constructor(props) {
        super(props);
        this.state = {
            value: props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({
                value: nextProps.value
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.value !== this.state.value;
    }

    render() {
        return <section className={css.root}>
            {this.renderInput('ru', 'russian')}
            {this.renderInput('us', 'english')}
        </section>;
    }

    renderInput(country, lang) {
        return <div className={css.lang}>
            <Flag country={country} />
            <div className={css.controls}>
                <RichEditor
                    ref={lang}
                    value={this.state.value.get(lang)}
                    onChange={val => this.handleChange(val, lang)}
                />
            </div>
        </div>;
    }

    handleChange(val, lang) {
        const value = this.state.value.set(lang, val);
        this.setState({ value });
        this.props.onChange(value);
    }
}
