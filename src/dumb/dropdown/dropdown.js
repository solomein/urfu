import React, { Component, PropTypes } from 'react';
import DropDownMenu from 'material-ui/lib/DropDownMenu';
import MenuItem from 'material-ui/lib/menus/menu-item';
import { ControlLabel } from 'dumb/control-label';
import css from './dropdown.css';

const { func, string, array, number, oneOfType } = PropTypes;

export default class Dropdown extends Component {
    static propTypes = {
        onChange: func,
        value: oneOfType([string, number]),
        data: array,
        dataConverter: func,
        label: string,
        maxHeight: number
    };

    static defaultProps = {
        dataConverter: data => data
    };

    render() {
        return <section>
            <ControlLabel>{this.props.label}</ControlLabel>
            <div className={css.control}>
                <DropDownMenu
                    maxHeight={this.props.maxHeight}
                    value={this.props.value}
                    onChange={(ev, i, val) => this.handleChange(val)}>
                    {
                        this.props.data.map(item => {
                            const data = this.props.dataConverter(item);
                            return <MenuItem
                                key={data.value}
                                value={data.value}
                                primaryText={data.text}
                            />;
                        })
                    }
                </DropDownMenu>
            </div>
        </section>;
    }

    handleChange(val) {
        this.props.onChange(val);
    }
}
