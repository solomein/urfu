import React, { Component, PropTypes } from 'react';
import { withMods } from 'utils/cssm';
import css from './week.css';
import { Set } from 'immutable';
import { isUndefined } from 'lodash/lang';

const mod = withMods(css);
const { array, func, object } = PropTypes;
const stringDays = [
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday',
    'Sunday'
];

export default class Week extends Component {
    static propTypes = {
        onChange: func.isRequired,
        days: array,
        value: object
    };

    static defaultProps = {
        days: ['ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВС'],
        value: Set()
    };

    constructor(props) {
        super(props);
        this.state = { value: Set(props.value) };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({ value: Set(nextProps.value) });
        }
    }

    render() {
        return <section className={css.root}>
            {this.props.days.map((day, i) => {
                return this.renderDay(day, i);
            })}
        </section>;
    }

    renderDay(day, i) {
        const active = !isUndefined(this.state.value.get(stringDays[i]));
        return <div
            className={mod.day({ active })}
            onClick={ev => this.handleDayClick(ev, i, active)}
            key={i}>
            {day}
        </div>;
    }

    handleDayClick(ev, i, active) {
        const value = active
            ? this.state.value.delete(stringDays[i])
            : this.state.value.add(stringDays[i]);

        this.setState({ value });
        this.props.onChange(value);
    }
}
