import React, { Component, PropTypes } from 'react';
import Week from './week';
import Time from './time';
import css from './work-time.css';
import LangInput from 'dumb/lang-input';
import { List, fromJS } from 'immutable';

const { string, func, object, oneOfType, date } = PropTypes;

export default class WorkTime extends Component {
    static propTypes = {
        startTime: oneOfType([string, date]),
        endTime: oneOfType([string, date]),
        weekDays: object,
        description: object,
        onChange: func.isRequired
    };

    static defaultProps = {
        weekDays: List()
    };

    calcState(props) {
        return {
            value: fromJS({
                startTime: props.startTime || this.getDate(),
                endTime: props.endTime || this.getDate(),
                weekDays: props.weekDays,
                description: props.description
            })
        }
    }

    getDate() {
        let date = new Date();
        date.setHours(0, 0, 0, 0);
        return date;
    }

    constructor(props) {
        super(props);

        this.state = this.calcState(props);
    }

    componentWillReceiveProps(nextProps) {
        this.setState(this.calcState(nextProps));
    }

    render() {
        const d = this.state.value;

        return <section className={css.root}>
            <div className={css.time}>
                <div className={css.timeTitle}>с</div>
                <div className={css.timePicker}>
                    <Time
                        value={d.get('startTime')}
                        onChange={this.handleStartTimeChange.bind(this)}
                    />
                </div>
                <div className={css.timeTitle}>по</div>
                <div className={css.timePicker}>
                    <Time
                        value={d.get('endTime')}
                        onChange={this.handleEndTimeChange.bind(this)}
                    />
                </div>
                <Week
                    value={d.get('weekDays')}
                    onChange={this.handleWeekChange.bind(this)}
                />
            </div>
            <LangInput
                value={d.get('description')}
                placeholder="Описание"
                multiline={true}
                direction="row"
                onChange={this.handleDescriptionChange.bind(this)}
            />
        </section>;
    }

    handleStartTimeChange(val) {
        const value = this.state.value.set('startTime', val);
        this.setState({ value });
        this.props.onChange(value);
    }

    handleEndTimeChange(val) {
        const value = this.state.value.set('endTime', val);
        this.setState({ value });
        this.props.onChange(value);
    }

    handleWeekChange(val) {
        const value = this.state.value.set('weekDays', val);
        this.setState({ value });
        this.props.onChange(value);
    }

    handleDescriptionChange(val) {
        const value = this.state.value.set('description', val);
        this.setState({ value });
        this.props.onChange(value);
    }
}
