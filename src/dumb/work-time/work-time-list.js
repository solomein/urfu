import React, { Component, PropTypes } from 'react';
import IconButton from 'material-ui/lib/icon-button';
import ActionDelete from 'material-ui/lib/svg-icons/action/delete';
import WorkTime from './work-time';
import css from './work-time-list.css';
import CardActionContainer from 'dumb/card-action-container';
import { List, fromJS } from 'immutable';

const { object, func } = PropTypes;

export default class WorkTimeList extends Component {
    static propTypes = {
        value: object,
        onChange: func.isRequired
    };

    static defaultProps = {
        value: List()
    };

    constructor(props) {
        super(props);

        this.state = {
            value: props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({ value: nextProps.value });
        }
    }

    render() {
        return <CardActionContainer
            actionLabel="Добавить время"
            onAction={() => this.handleAddWorkTime()}
        >
            <div className={css.list}>
                {this.renderWorkTimes()}
            </div>
        </CardActionContainer>;
    }

    renderWorkTimes() {
        return this.state.value.map((workTime, i) => {
            return <div className={css.item} key={i}>
                <WorkTime
                    startTime={workTime.get('startTime')}
                    endTime={workTime.get('endTime')}
                    weekDays={workTime.get('weekDays')}
                    description={workTime.get('description')}
                    onChange={value => this.handleWorkTimeChange(value, i)}
                />
                <div className={css.delete}>
                    <IconButton
                        tooltipPosition="top-left"
                        tooltip="Удалить время"
                        onTouchTap={ev => this.handleRemoveWorkTime(ev, i)}>
                        <ActionDelete />
                    </IconButton>
                </div>
            </div>}
        );
    }

    handleAddWorkTime() {
        const newTime = {
            description: {
                russian: '',
                english: ''
            },
            startTime: '',
            endTime: '',
            weekDays: []
        }
        this.setState({ value: this.state.value.push(fromJS(newTime)) });
    }

    handleRemoveWorkTime(ev, i) {
        const value = this.state.value.delete(i);
        this.setState({ value });
        this.props.onChange(value);
    }

    handleWorkTimeChange(val, i) {
        const value = this.state.value.set(i, val);
        this.setState({ value });
        this.props.onChange(value);
    }
}
