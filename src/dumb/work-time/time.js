import React, { Component, PropTypes } from 'react';
import TimePicker from 'material-ui/lib/time-picker/time-picker';

const { string, func, date, oneOfType } = PropTypes;

export default class Time extends Component {
    static propTypes = {
        value: oneOfType([string, date]),
        onChange: func.isRequired
    };

    componentWillReceiveProps(nextProps) {
        //HACK
        this.refs.picker.setState({
            time: this.getDate(nextProps.value),
            dialogTime: this.getDate(nextProps.value)
        })
    }

    render() {
        return <TimePicker
            ref="picker"
            format="24hr"
            hintText="00:00"
            defaultTime={this.getDate(this.props.value)}
            textFieldStyle={{width: 40}}
            onChange={this.handleChange.bind(this)}
        />;
    }

    handleChange(ev, val) {
        this.props.onChange(val.toISOString());
    }

    getDate(str) {
        if (str && str.length) {
            const base = new Date(str);

            if (base == 'Invalid Date') {
                return this.parseTime(str);
            }

            return base;
        }
        else {
            return this.getDateFromTime(0, 0);
        }
    }

    getDateFromTime(h, m) {
        let date = new Date();
        date.setHours(h, m, 0, 0);

        return date;
    }

    parseTime(time) {
        const index = time.indexOf(':');

        if (index < 0) {
            return this.getDateFromTime(0, 0);
        }

        const hh = time.slice(index - 2, index);
        const mm = time.slice(index + 1, index + 3);
        return this.getDateFromTime(hh, mm);
    }
}
