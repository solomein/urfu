import React, { Component, PropTypes } from 'react';
import css from './control-label.css';

const { string } = PropTypes;

export default class Label extends Component {
    static propTypes = {
        children: string
    };

    static defaultProps = {
        children: ''
    };

    shouldComponentUpdate() {
        return false;
    }

    render() {
        if (!this.props.children.length) {
            return null;
        }

        return <section className={css.root}>{this.props.children}</section>;
    }
}
