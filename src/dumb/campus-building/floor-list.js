import React, { Component, PropTypes } from 'react';
import IconButton from 'material-ui/lib/icon-button';
import ActionDelete from 'material-ui/lib/svg-icons/action/delete';
import Floor from './floor';
import css from './floor-list.css';
import { List, fromJS } from 'immutable';
import CardActionContainer from 'dumb/card-action-container';
import { map } from 'provider';
import FileUploader from 'utils/file-uploader';

const { object, func } = PropTypes;

export default class FloorList extends Component {
    static propTypes = {
        value: object,
        onChange: func.isRequired,
        onError: func.isRequired
    };

    static defaultProps = {
        value: List()
    };

    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value
        };

        this.uploader = new FileUploader({
            request: map.uploadMap,
            onUpload: this.handleMapUploadStart.bind(this),
            onSuccess: this.handleMapUploadSuccess.bind(this),
            onFail: this.handleMapUploadFail.bind(this),
            onAllComplete: () => {}
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({ value: nextProps.value });
        }
    }

    componentWillUnmount() {
        this.uploader.destroy();
    }

    render() {
        return <CardActionContainer
            actionLabel="Добавить карту этажа"
            onAction={() => this.handleFloorAdd()}
        >
            <div className={css.list}>
                {this.renderFloors()}
            </div>
        </CardActionContainer>;
    }

    renderFloors() {
        return this.state.value.map((floor, i) =>
            <div key={i} className={css.item}>
                <Floor
                    value={floor}
                    onChange={val => this.handleFloorChange(val, i)}
                    onImageClick={id => this.handleImageClick(id)}
                />
                <IconButton
                    tooltip="Удалить этаж"
                    tooltipPosition="top-center"
                    onTouchTap={ev => this.handleFloorRemove(ev, i)}>
                    <ActionDelete />
                </IconButton>
            </div>
        );
    }

    handleFloorAdd() {
        this.uploader.openDialog();
    }

    handleFloorRemove(ev, i) {
        const value = this.state.value.delete(i);

        this.setState({ value });
        this.props.onChange(value);
    }

    handleFloorChange(val, i) {
        const currentNumber = this.state.value.getIn([i, 'number']);

        const value = this.state.value.map(floor => {
            return floor.get('number') === val.number
                ? floor.set('number', currentNumber)
                : floor;
        }).mergeIn([i], val);

        this.setState({ value });
        this.props.onChange(value);
    }

    handleMapUploadStart() { }

    handleMapUploadSuccess(file) {
        const maxFloor = this.state.value.maxBy(floor => {
            return floor.get('number');
        });
        const floor = fromJS({
            number: maxFloor ? maxFloor.get('number') + 1 : 1,
            map: file
        });
        const value = this.state.value.push(floor);

        this.setState({ value });
        this.props.onChange(value);
    }

    handleMapUploadFail(file, error) {
        this.props.onError(error.message);
    }

    handleImageClick(id) {
        map.downloadMap(id);
    }
}
