import React from 'react';
import LangInput from 'dumb/lang-input';
import FormGroup from 'dumb/form-group';
import Card from 'dumb/card';
import { langTextIsNotEmpty } from 'helpers/validate';
import FloorList from './floor-list';
import Form from 'dumb/form';

export default class CampusBuilding extends Form {
    validate = [
        {
            name: 'Адрес',
            dataField: 'title',
            stateField: 'titleIsValid',
            validator: langTextIsNotEmpty
        }
    ];

    constructor(props) {
        super(props);
        this.state = this.calcState(props.data);
    }

    renderContent(d) {
        return (
            <div>
                <Card>
                    <FormGroup title="Адрес" expanded={true}>
                        <LangInput
                            required
                            value={d.get('title')}
                            placeholder="Адрес"
                            direction="row"
                            onChange={val => this.handlePropChange('title', val)}
                        />
                    </FormGroup>
                </Card>
                <Card>
                    <FormGroup title="Этажи" expanded>
                        <FloorList
                            value={d.get('floors')}
                            onChange={val => this.handlePropChange('floors', val)}
                            onError={val => this.handleError(val)}
                        />
                    </FormGroup>
                </Card>
            </div>
        );
    }

    handleError(val) {
        this.props.onError(val);
    }
}
