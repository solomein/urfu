import React, { Component, PropTypes } from 'react';
import Image from 'material-ui/lib/svg-icons/image/image';
import css from './floor.css';
import Dropdown from 'dumb/dropdown';

const floorNumbers = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
const { object, func } = PropTypes;

export default class Floor extends Component {
    static propTypes = {
        value: object,
        onChange: func.isRequired,
        onImageClick: func.isRequired
    };

    static defaultProps = {
    };

    render() {
        return <section className={css.root}>
            Этаж
            <div className={css.number}>
                <Dropdown
                    maxHeight={300}
                    data={floorNumbers}
                    value={this.props.value.get('number') || 1}
                    onChange={val => this.handleNumberChange(val)}
                    dataConverter={number => {
                        return { id: number, value: number, text: number }
                    }}
                />
            </div>
            <div className={css.image} onClick={() => this.handleImageClick()}>
                <Image color="#e0e0e0"/>
                <div className={css.imageName}>
                    {this.props.value.getIn(['map', 'name'])}
                </div>
            </div>
        </section>;
    }

    handleNumberChange(value) {
        this.props.onChange({ number: value });
    }

    handleImageClick() {
        this.props.onImageClick(this.props.value.getIn(['map', 'id']));
    }
}
