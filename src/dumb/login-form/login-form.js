import React, { Component, PropTypes } from 'react';
import RaisedButton from 'material-ui/lib/raised-button';
import { TextField } from 'dumb/text-field';
import Card from 'dumb/card';

import css from './login-form.css';

const { func } = PropTypes;

export default class CategoryList extends Component {
    static propTypes = {
        onSubmit: func.isRequired
    };

    state = {
        login: '',
        password: ''
    };

    render() {
        return <Card>
            <div className={css.form}>
                <div className={css.logo}></div>
                <div className={css.input}>
                    <TextField
                        value={this.state.login}
                        placeholder="Логин"
                        onChange={val => this.handleLoginChange(val)}
                        onKeyPress={ev => this.handleKeyPress(ev)}
                    />
                </div>
                <div className={css.input}>
                    <TextField
                        value={this.state.password}
                        placeholder="Пароль"
                        type="password"
                        onChange={val => this.handlePasswordChange(val)}
                        onKeyPress={ev => this.handleKeyPress(ev)}
                    />
                </div>
                <div className={css.button}>
                    <RaisedButton
                        label="войти"
                        primary={true}
                        onTouchTap={this.handleSubmit.bind(this)}
                        disabled={!(this.state.password && this.state.login)}
                    />
                </div>
            </div>
        </Card>;
    }

    handleLoginChange(login) {
        this.setState({ login });
    }

    handlePasswordChange(password) {
        this.setState({ password });
    }

    handleKeyPress(ev) {
        if (ev.key === 'Enter') {
            this.handleSubmit();
        }
    }

    handleSubmit() {
        this.props.onSubmit(this.state.login, this.state.password);
    }
}
