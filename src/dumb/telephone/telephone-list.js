import React, { Component, PropTypes } from 'react';
import CardActionContainer from 'dumb/card-action-container';
import IconButton from 'material-ui/lib/icon-button';
import ActionDelete from 'material-ui/lib/svg-icons/action/delete';
// import { isUndefined } from 'lodash/lang';
import Telephone from './telephone';
import css from './telephone-list.css';
import { List } from 'immutable';

const { object, func } = PropTypes;

export default class TelephoneList extends Component {
    static propTypes = {
        value: object,
        onChange: func.isRequired
    };

    static defaultProps = {
        value: List()
    };

    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({ value: nextProps.value });
        }
    }

    render() {
        return <CardActionContainer
            actionLabel="Добавить телефон"
            onAction={() => this.handleAddNumber()}
        >
            <div className={css.list}>
                {this.renderNumbers()}
            </div>
        </CardActionContainer>;
    }

    renderNumbers() {
        return this.state.value.map((number, i) =>
            <div className={css.item} key={i}>
                <Telephone
                    number={number}
                    onChange={value => this.handleNumberChange(value, i)}
                />
                <IconButton
                    tooltipPosition="top-center"
                    tooltip="Удалить телефон"
                    onTouchTap={ev => this.handleRemoveNumber(ev, i)}>
                    <ActionDelete />
                </IconButton>
            </div>
        );
    }

    handleAddNumber() {
        this.setState({ value: this.state.value.push('') });
    }

    handleRemoveNumber(ev, i) {
        const value = this.state.value.delete(i);

        this.setState({ value });
        this.props.onChange(value);
    }

    handleNumberChange(val, i) {
        const value = this.state.value.set(i, val);
        //this.state.value[i] = value;
        this.setState({ value });
        this.props.onChange(value);
    }
}
