import React, { Component, PropTypes } from 'react';
import css from './telephone.css';
import { TextField } from 'dumb/text-field';

const { string, func } = PropTypes;

export default class Telephone extends Component {
    static propTypes = {
        number: string,
        onChange: func.isRequired
    };

    static defaultProps = {
        number: ''
    };

    render() {
        return <section className={css.root}>
            <TextField
                placeholder="Номер телефона"
                value={this.props.number}
                onChange={this.handleNumberChange.bind(this)}
            />
        </section>;
    }

    handleNumberChange(value) {
        this.props.onChange(value);
    }
}
