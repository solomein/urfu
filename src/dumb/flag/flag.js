import React, { Component, PropTypes } from 'react';
import { withMods } from 'utils/cssm';
import css from './flag.css';

const mod = withMods(css);
const { string } = PropTypes;

export default class Flag extends Component {
    static propTypes = {
        country: string
    };

    shouldComponentUpdate() {
        return false;
    }

    render() {
        return <section className={mod.root({ country: this.props.country })}>
        </section>;
    }
}
