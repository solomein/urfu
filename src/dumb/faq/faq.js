import React from 'react';
import LangInput from 'dumb/lang-input';
import FormGroup from 'dumb/form-group';
import Card from 'dumb/card';
import { langTextIsNotEmpty } from 'helpers/validate';
import AdditionalInfo from 'dumb/additional-info';
import Form from 'dumb/form';

export default class Faq extends Form {
    validate = [
        {
            name: 'Вопрос',
            dataField: 'question',
            stateField: 'questionIsValid',
            validator: langTextIsNotEmpty
        },
        {
            name: 'Ответ',
            dataField: 'answer',
            stateField: 'answerIsValid',
            validator: langTextIsNotEmpty
        }
    ];

    constructor(props) {
        super(props);
        this.state = this.calcState(props.data);
    }

    renderContent(d) {
        return (
            <div>
                <Card>
                    <FormGroup title="Вопрос" expanded>
                        <LangInput
                            required
                            value={d.get('question')}
                            placeholder="Вопрос"
                            direction="row"
                            onChange={val => this.handlePropChange('question', val)}
                        />
                    </FormGroup>
                    <FormGroup title="Ответ" expanded>
                        <LangInput
                            required
                            value={d.get('answer')}
                            placeholder="Ответ"
                            multiline={true}
                            onChange={val => this.handlePropChange('answer', val)}
                        />
                    </FormGroup>
                </Card>

                <AdditionalInfo
                    data={{
                        additionalText: d.get('additionalText'),
                        pageLink: d.get('pageLink'),
                        externalLink: d.get('externalLink'),
                        telephone: d.get('telephone'),
                        contacts: d.get('contacts')
                    }}
                    onChange={val => this.handleAdditionalInfoChange(val)}
                />
            </div>
        );
    }

    handleAdditionalInfoChange(value) {
        this.setState({
            data: this.state.data.merge(value)
        });
    }
}
