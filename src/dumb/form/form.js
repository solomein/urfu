import React, { Component, PropTypes } from 'react';
import RaisedButton from 'material-ui/lib/raised-button';
import css from './form.css';
import AlertErrorOutline from 'material-ui/lib/svg-icons/alert/error-outline';
import { Map } from 'immutable';
import find from 'lodash/find';

const { object, func } = PropTypes;

export default class Form extends Component {
    static propTypes = {
        data: object,
        onSave: func
    };

    static defaultProps = {
        data: Map()
    };

    //validate = [];

    calcState(data) {
        let state = { data };
        this.validate.forEach(v => {
            state[v.stateField] = v.validator(data.get(v.dataField));
        });

        return state;
    }

    stateIsValid() {
        return this.validate.every(v => this.state[v.stateField]);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data) {
            this.setState(this.calcState(nextProps.data));
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextProps.data !== this.props.data
            || nextState.data !== this.state.data
            || this.validate.some(
                v => this.state[v.stateField] !== nextState[v.stateField]
            );
    }

    render() {
        return <section className={css.root}>
            {this.renderContent(this.state.data)}
            <div className={css.controls}>
                 <RaisedButton
                     disabled={!this.stateIsValid()}
                     primary={true}
                     label="Сохранить"
                     onTouchTap={() => this.handleSave()}
                 />
                 {this.renderMessage()}
            </div>
        </section>;
    }

    renderContent() {}

    renderMessage() {
        if (this.stateIsValid()) {
            return;
        }

        return <div className={css.message}>
            <AlertErrorOutline color="rgba(0, 0, 0, .26)"/>
            <div className={css.messageText}>
                {this.getTextMessage()}
            </div>
        </div>;
    }

    getTextMessage() {
        let fields = [];

        this.validate.forEach(v => {
            if (!this.state[v.stateField]) {
                fields.push(v.name);
            }
        });

        const last = fields.pop();

        let fieldsText = fields.join(', ');

        if (fieldsText.length) {
            fieldsText += ' и ';
        }

        return `Заполните ${fieldsText += last}`;
    }

    handlePropChange(propName, value) {
        const state = {
            data: this.state.data.set(propName, value)
        };

        const validate = find(this.validate, { dataField: propName });

        if (validate) {
            state[validate.stateField] = validate.validator(value);
        }

        this.setState(state)
    }

    handleAdditionalInfoChange(value) {
        this.setState({
            data: this.state.data.merge(value)
        });
    }

    handleSave() {
        this.props.onSave(this.state.data);
    }

    getData() {
        return this.state.data;
    }
}
