import React from 'react';
import LangInput from 'dumb/lang-input';
import LangAddress from 'dumb/lang-address';
import LangRichText from 'dumb/lang-rich-text';
import FormGroup from 'dumb/form-group';
import Card from 'dumb/card';
import ColorPicker from 'dumb/color-picker';
import { WorkTimeList } from 'dumb/work-time';
import { TelephoneList } from 'dumb/telephone';
import ImagesList from 'dumb/images-list';
import css from './place.css';
import { langTextIsNotEmpty, addressIsValid } from 'helpers/validate';
import { getColors, getGradients } from 'provider';
import Form from 'dumb/form';

export default class Place extends Form {
    validate = [
        {
            name: 'Название',
            dataField: 'title',
            stateField: 'titleIsValid',
            validator: langTextIsNotEmpty
        },
        {
            name: 'Адрес',
            dataField: 'address',
            stateField: 'addressIsValid',
            validator: addressIsValid
        },
        {
            name: 'Цвет шапки',
            dataField: 'gradient',
            stateField: 'gradientIsValid',
            validator: v => {
                return !this.props.canEditGradient || this.validateGradient(v);
            }
        }
    ];

    constructor(props) {
        super(props);
        this.state = this.calcState(props.data);
    }

    renderContent(d) {
        return (
            <div>
                <Card>
                    <FormGroup title="Название" expandable={false}>
                        <LangInput
                            required
                            value={d.get('title')}
                            placeholder="Название"
                            direction="row"
                            onChange={val => this.handlePropChange('title', val)}
                        />
                    </FormGroup>
                    <FormGroup title="Краткое описание">
                        <LangInput
                            value={d.get('shortDescription')}
                            placeholder="Описание"
                            multiline={true}
                            onChange={val => this.handlePropChange('shortDescription', val)}
                        />
                    </FormGroup>
                    <FormGroup
                        title="Информация"
                    >
                        <LangRichText
                            ref={c => this._refDescrition = c}
                            value={d.get('description')}
                            onChange={val => this.handlePropChange('description', val)}
                        />
                    </FormGroup>
                </Card>
                <Card>
                    <FormGroup title="Адрес">
                        <LangAddress
                            value={d.get('address')}
                            onChange={val => this.handlePropChange('address', val)}
                        />
                    </FormGroup>
                </Card>
                <Card>
                    <FormGroup title="Время работы">
                        <WorkTimeList
                            value={d.get('workTimes')}
                            onChange={val => this.handlePropChange('workTimes', val)}
                        />
                    </FormGroup>
                </Card>
                <Card>
                    <FormGroup title="Телефоны">
                        <TelephoneList
                            value={d.get('telephones')}
                            onChange={val => this.handlePropChange('telephones', val)}
                        />
                    </FormGroup>
                </Card>
                {console.log(d.get('images').toJS())}
                <Card>
                    <FormGroup title="Изображения">
                        <ImagesList
                            value={d.get('images')}
                            onChange={val => this.handlePropChange('images', val)}
                        />
                    </FormGroup>
                </Card>
                {this.renderColors(d)}
            </div>
        );
    }

    renderColors(d) {
        const { canEditColor, canEditGradient } = this.props;

        if (!canEditColor && !canEditGradient) {
            return;
        }

        return <Card>
            <FormGroup title="Цвета">
                <div className={css.colors}>
                    {canEditColor && <div className={css.color}>
                        <ColorPicker
                            color={d.get('color')}
                            onChange={val => this.handleColorChange(val)}
                            colors={getColors()}
                            title="Цвет иконки"
                        />
                    </div>}
                    {canEditGradient && <div className={css.color}>
                         <ColorPicker
                            from={d.getIn(['gradient', 'from'])}
                            to={d.getIn(['gradient', 'to'])}
                            onChange={val => this.handleGradientChange(val)}
                            colors={getGradients()}
                            title="Цвет шапки"
                        />
                    </div>}
                </div>
            </FormGroup>
        </Card>
    }

    handleColorChange(value) {
        this.setState({
            data: this.state.data.set('color', value.color)
        });
    }

    handleGradientChange(value) {
        this.setState({
            data: this.state.data.merge({ gradient: value }),
            gradientIsValid: this.validateGradient(value)
        });
    }

    validateGradient(gradient) {
        if (!gradient) {
            return false;
        }

        if (gradient.size) {
            return gradient.get('from') && gradient.get('to');
        }

        return gradient.from && gradient.to;
    }
}
