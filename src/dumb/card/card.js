import React, { Component, PropTypes } from 'react';
import Paper from 'material-ui/lib/paper';
import css from './card.css';

export default class Card extends Component {
    render() {
        return (
            <section className={css.root}>
                <Paper zDepth={1}>
                    {this.props.children}
                </Paper>
            </section>
        );
    }
}
