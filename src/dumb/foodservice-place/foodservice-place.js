import React, { PropTypes } from 'react';
import FlatButton from 'material-ui/lib/flat-button';
import LangAddress from 'dumb/lang-address';
import FormGroup from 'dumb/form-group';
import Card from 'dumb/card';
import { addressIsValid } from 'helpers/validate';
import EntityList from 'dumb/entity-list';
import Form from 'dumb/form';

const { object, func } = PropTypes;

export default class FoodservicePlace extends Form {
    static propTypes = {
        data: object,
        onSave: func,
        onFoodserviceSelect: func,
        onFoodserviceDelete: func,
        onFoodserviceAdd: func
    };

    validate = [
        {
            name: 'Адрес',
            dataField: 'address',
            stateField: 'addressIsValid',
            validator: addressIsValid
        }
    ];

    constructor(props) {
        super(props);
        this.state = this.calcState(props.data);
    }

    renderContent(d) {
        return (
            <div>
                <Card>
                    <FormGroup title="Адрес">
                        <LangAddress
                            value={d.get('address')}
                            onChange={val => this.handlePropChange('address', val)}
                        />
                    </FormGroup>
                </Card>
                {this.renderFoodservices(d.get('foodservices'))}
                <FlatButton
                    label="Добавить заведение"
                    onTouchTap={() => this.handleFoodserviceAdd()}
                />
            </div>
        );
    }

    renderFoodservices(foodservices) {
        if (!foodservices || !foodservices.size) {
            return;
        }

        return <Card>
            <EntityList
                data={foodservices}
                onSelect={(data, i) => this.handleFoodserviceSelect(i)}
                onDelete={(data, i) => this.handleFoodserviceDelete(i)}
                onMoveEnd={() => {}}
                onMove={data => this.handleFoodserviceMove(data)}
                dataConverter={data => {
                    return {
                        id: data.get('id') || data.get('_cid'),
                        primaryText: data.getIn(['title', 'russian'])
                    }
                }}
            />
        </Card>;
    }

    handleFoodserviceAdd() {
        this.props.onFoodserviceAdd();
    }

    handleFoodserviceSelect(data) {
        this.props.onFoodserviceSelect(data);
    }

    handleFoodserviceDelete(data) {
        this.props.onFoodserviceDelete(data);
    }

    handleFoodserviceMove(data) {
        this.props.onFoodserviceMove(data);
    }
}
