import React from 'react';
import LangInput from 'dumb/lang-input';
import FormGroup from 'dumb/form-group';
import Card from 'dumb/card';
import { WorkTimeList } from 'dumb/work-time';
import { langTextIsNotEmpty } from 'helpers/validate';
import Form from 'dumb/form';
import { getFoodserviceTypes } from 'provider';
import Dropdown from 'dumb/dropdown';

export default class Foodservice extends Form {
    validate = [
        {
            name: 'Название',
            dataField: 'title',
            stateField: 'titleIsValid',
            validator: langTextIsNotEmpty
        }
    ];

    constructor(props) {
        super(props);
        this.state = this.calcState(props.data);
    }

    renderContent(d) {
        const types = getFoodserviceTypes();

        return (
            <div>
                <Card>
                    <FormGroup title="Название" expanded>
                        <div style={{ marginBottom: 24 }}>
                            <LangInput
                                required
                                value={d.get('title')}
                                placeholder="Название"
                                direction="row"
                                onChange={val => this.handlePropChange('title', val)}
                            />
                        </div>
                        <Dropdown
                            label="Тип"
                            data={types}
                            value={d.get('type') || types[0].value}
                            onChange={val => this.handlePropChange('type', val)}
                        />
                    </FormGroup>
                </Card>
                <Card>
                    <FormGroup title="Время работы" expanded>
                        <WorkTimeList
                            value={d.get('workTimes')}
                            onChange={val => this.handlePropChange('workTimes', val)}
                        />
                    </FormGroup>
                </Card>
            </div>
        );
    }
}
