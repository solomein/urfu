import React, { Component, PropTypes } from 'react';
import ExpandLess from 'material-ui/lib/svg-icons/navigation/expand-less';
import ExpandMore from 'material-ui/lib/svg-icons/navigation/expand-more';
import { withMods } from 'utils/cssm';
import Divider from 'material-ui/lib/divider';
import IconButton from 'material-ui/lib/icon-button';
import ActionDelete from 'material-ui/lib/svg-icons/action/delete';

import css from './form-group.css';

const { string, bool, func } = PropTypes;
const mod = withMods(css);

export default class FormGroup extends Component {
    static propTypes = {
        title: string.isRequired,
        subtitle: string,
        expanded: bool,
        expandable: bool,
        onExpand: func,
        onDelete: func,
        canDelete: bool
    };

    static defaultProps = {
        subtitle: '',
        expanded: false,
        expandable: true
    };

    constructor(props) {
        super(props);

        this.state = {
            expanded: this.props.expanded || !this.props.expandable
        };
        this.wasExpanded = this.state.expanded;
    }

    componentWillUpdate(nextProps, nextState) {
        this.wasExpanded = this.wasExpanded || nextState.expanded;
    }

    componentDidUpdate() {
        if (this.state.expanded && this.props.onExpand && this.manualExpand) {
            this.manualExpand = false;
            this.props.onExpand();
        }
    }

    render() {
        return (
            <section className={css.root}>
                {
                    this.props.expandable
                        ? this.renderHeader()
                        : this.renderStaticHeader()
                }
                {this.renderContent()}
                <Divider />
            </section>
        );
    }

    renderContent() {
        const expanded = this.state.expanded;

        if (!this.wasExpanded) {
            return;
        }

        return <div className={mod.content({ expanded })}>
            {this.props.children}
        </div>
    }

    renderStaticHeader() {
        return (
            <div className={css.staticHeader}>
                <div className={css.title}>{this.props.title}</div>
                <div className={css.subtitle}>{this.props.subtitle}</div>
                {this.renderDeleteButton()}
            </div>
        );
    }

    renderHeader() {
        const icon = this.state.expanded
            ? <ExpandLess />
            : <ExpandMore />;
        return (
            <div className={css.header} onClick={this.handleToggle.bind(this)}>
                <div className={css.title}>{this.props.title}</div>
                <div className={css.subtitle}>{this.props.subtitle}</div>
                {this.renderDeleteButton()}
                <div>{icon}</div>
            </div>
        );
    }

    renderDeleteButton() {
        if (!this.props.canDelete) {
            return;
        }

        return <IconButton
            tooltipPosition="top-center"
            tooltip="Удалить"
            onTouchTap={ev => this.handleDelete(ev)}>
            <ActionDelete />
        </IconButton>
    }

    handleToggle() {
        this.manualExpand = true;
        this.setState({ expanded: !this.state.expanded });
    }

    handleDelete() {
        this.props.onDelete();
    }
}
