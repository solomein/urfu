import React, { Component, PropTypes } from 'react';
import { withMods } from 'utils/cssm';
import css from './color-label.css';

const { string, func } = PropTypes;
const mod = withMods(css);

export default class ColorLabel extends Component {
    static propTypes = {
        color: string,
        from: string,
        to: string,
        onClick: func
    };

    render() {
        const { color, from, to } = this.props;
        const style = {
            backgroundColor: color,
            backgroundImage: `linear-gradient(${from}, ${to})`
        };

        return <div
            onClick={ev => this.handleClick(ev)}
            className={mod.root({ empty: this.isEmpty(), small: this.props.small })}
            style={this.isEmpty() ? {} : style}
        >
            {this.renderName()}
        </div>;
    }

    renderName() {
        if (this.isEmpty()) {
            return 'Нет';
        }
    }

    isEmpty() {
        const { color, from, to } = this.props;
        return !color && !(from && to);
    }

    handleClick(ev) {
        this.props.onClick(ev, {
            color: this.props.color,
            from: this.props.from,
            to: this.props.to
        });
    }
}