import React, { PropTypes, Component } from 'react';
import Popover from 'material-ui/lib/popover/popover';
import PopoverAnimationFromTop from 'material-ui/lib/popover/popover-animation-from-top';
import ColorLabel from './color-label';
import { ControlLabel } from 'dumb/control-label';
import css from './color-picker.css';

const { array, func, string } = PropTypes;

export default class PopoverExampleAnimation extends Component {
    static propTypes = {
        colors: array,
        onChange: func,
        color: string,
        from: string,
        to: string,
        title: string
    };

    static defaultProps = {
        colors: []
    };

    constructor(props) {
        super(props);

        this.state = {
            open: false,
            color: this.props.color,
            from: this.props.from,
            to: this.props.to
        };
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            color: nextProps.color,
            from: nextProps.from,
            to: nextProps.to
        });
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.open !== this.state.open;
    }

    render() {
        return (
            <div>
                <ControlLabel>{this.props.title}</ControlLabel>
                <ColorLabel
                    onClick={ev => this.handleTouchTap(ev)}
                    color={this.state.color}
                    from={this.state.from}
                    to={this.state.to}
                />
                <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{ horizontal: 'left', vertical: 'top' }}
                    targetOrigin={{ horizontal: 'left', vertical: 'bottom' }}
                    onRequestClose={ev => this.handleRequestClose(ev)}
                    animation={PopoverAnimationFromTop}
                >
                    {this.renderVariants()}
                </Popover>
            </div>
        );
    }

    renderVariants() {
        return <div className={css.variants}>
            {this.props.colors.map((color, i) => {
                return <div className={css.variant} key={i}>
                    <ColorLabel
                        small
                        color={color.color}
                        from={color.from}
                        to={color.to}
                        onClick={() => this.handleVariantClick(color)}
                    />
                </div>;
            })}
        </div>;
    }

    handleVariantClick(data) {
        this.setState(Object.assign({}, this.state, {
            color: data.color,
            from: data.from,
            to: data.to,
            open: false
        }));

        this.props.onChange(data);
    }

    handleTouchTap(ev) {
        this.setState({
            open: true,
            anchorEl: ev.currentTarget
        });
    }

    handleRequestClose() {
        this.setState({
            open: false
        });
    }
}
