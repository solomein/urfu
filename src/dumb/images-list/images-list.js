import React, { Component, PropTypes } from 'react';
import IconButton from 'material-ui/lib/icon-button';
import ActionDelete from 'material-ui/lib/svg-icons/action/delete';
import Image from './image';
import css from './images-list.css';
import { List, fromJS } from 'immutable';
import CardActionContainer from 'dumb/card-action-container';
import { place } from 'provider';
import FileUploader from 'utils/file-uploader';

const { object, func } = PropTypes;

export default class ImagesList extends Component {
    static propTypes = {
        value: object,
        onChange: func.isRequired
    };

    static defaultProps = {
        value: List()
    };

    constructor(props) {
        super(props);

        this.state = {
            value: this.props.value
        };

        this.uploader = new FileUploader({
            request: place.uploadImage,
            onUpload: this.handleImageUploadStart.bind(this),
            onSuccess: this.handleImageUploadSuccess.bind(this),
            onFail: this.handleImageUploadFail.bind(this),
            onAllComplete: () => {}
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({ value: nextProps.value });
        }
    }

    componentWillUnmount() {
        this.uploader.destroy();
    }

    render() {
        return <CardActionContainer
            actionLabel="Добавить изображение"
            onAction={() => this.handleFloorAdd()}
        >
            <div className={css.list}>
                {this.renderImages()}
            </div>
        </CardActionContainer>;
    }

    renderImages() {
        return this.state.value.map((image, i) =>
            <div key={i} className={css.item}>
                <Image
                    value={image}
                    onChange={val => this.handleImageChange(val, i)}
                    onImageClick={id => this.handleImageClick(id)}
                />
                <IconButton
                    tooltip="Удалить изображение"
                    tooltipPosition="top-center"
                    onTouchTap={ev => this.handleImageRemove(ev, i)}>
                    <ActionDelete />
                </IconButton>
            </div>
        );
    }

    handleFloorAdd() {
        this.uploader.openDialog();
    }

    handleImageRemove(ev, i) {
        const value = this.state.value.delete(i);

        this.setState({ value });
        this.props.onChange(value);
    }

    handleImageChange(val, i) {
        const value = this.state.value.mergeIn([i], val);

        this.setState({ value });
        this.props.onChange(value);
    }

    handleImageUploadStart() { }

    handleImageUploadSuccess(file) {
        const image = fromJS({
            id: file.id,
            fileName: file.name,
            description: ''
        });

        const value = this.state.value.push(image);

        this.setState({ value });
        this.props.onChange(value);
    }

    handleImageUploadFail() { }

    handleImageClick(id) {
        place.downloadImage(id);
    }
}
