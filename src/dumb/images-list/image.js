import React, { Component, PropTypes } from 'react';
import Image from 'material-ui/lib/svg-icons/image/image';
import css from './image.css';
import { TextField } from 'dumb/text-field';

const { object, func } = PropTypes;

export default class Floor extends Component {
    static propTypes = {
        value: object,
        onChange: func.isRequired,
        onImageClick: func.isRequired
    };

    static defaultProps = {
    };

    render() {
        return <section className={css.root}>
            <div className={css.image} onClick={() => this.handleImageClick()}>
                <Image color="#e0e0e0"/>
                <div className={css.imageName}>
                    {this.props.value.get('fileName')}
                </div>
            </div>
            <div className={css.description}>
                <TextField
                    placeholder="Описание"
                    value={this.props.value.get('description')}
                    onChange={this.handleDescriptionChange.bind(this)}
                />
            </div>
        </section>;
    }

    handleDescriptionChange(value) {
        this.props.onChange({ description: value });
    }

    handleImageClick() {
        this.props.onImageClick(this.props.value.get('id'));
    }
}
