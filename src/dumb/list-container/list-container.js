import React from 'react';
import Paper from 'material-ui/lib/paper';
import css from './list-container.css';

export default function ListContainer(props) {
    return (
        <section className={css.root}>
            <Paper style={{ width: '100%' }} zDepth={1}>
                {props.children}
            </Paper>
        </section>
    );
}
