import React, { Component } from 'react';
import { Motion, spring } from 'react-motion'

export default class Form extends Component {
    render() {
        return <Motion
            defaultStyle={{ opacity: 0, marginTop: -24 }}
            style={{
                opacity: spring(1, { stiffness: 240, damping: 24 }),
                marginTop: spring(0, { stiffness: 240, damping: 24 })
            }}
        >
            {interpolatingStyle =>
                <div style={interpolatingStyle}>
                    {this.props.children}
                </div>
            }
        </Motion>
    }
}
