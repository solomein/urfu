import React, { Component, PropTypes } from 'react';
import LangInput from 'dumb/lang-input';
import LangRichText from 'dumb/lang-rich-text';
import { Map } from 'immutable';
import css from './additional-info.css';

const { object, func } = PropTypes;

export default class AdditionalText extends Component {
    static propTypes = {
        value: object,
        onChange: func
    };

    static defaultProps = {
        value: Map()
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({
                value: nextProps.value
            });
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            value: props.value
        };
    }

    render() {
        return (
            <div>
                <div className={css.title}>
                    <LangInput
                        required
                        value={this.state.value.get('title')}
                        placeholder="Заголовок"
                        direction="row"
                        onChange={val => this.handleTitleChange(val)}
                    />
                </div>
                <LangRichText
                    ref="text"
                    value={this.state.value.get('text')}
                    onChange={val => this.handleTextChange(val)}
                />
            </div>
        );
    }

    handleTitleChange(val) {
        const value = this.state.value.set('title', val);
        this.setState({ value });
        this.props.onChange(value);
    }

    handleTextChange(val) {
        const value = this.state.value.set('text', val);
        this.setState({ value });
        this.props.onChange(value);
    }
}
