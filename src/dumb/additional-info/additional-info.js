import React, { Component, PropTypes } from 'react';
import IconMenu from 'material-ui/lib/menus/icon-menu';
import MenuItem from 'material-ui/lib/menus/menu-item';
import FlatButton from 'material-ui/lib/flat-button';
import FormGroup from 'dumb/form-group';
import Card from 'dumb/card';
import AdditionalText from './additional-text';
import AdditionalTelephone from './additional-telephone';
import AdditionalLink from './additional-link';
import AdditionalPage from './additional-page';
import LangAddress from 'dumb/lang-address';
import { Map, fromJS } from 'immutable';

const { object, func } = PropTypes;

export default class AdditionalInfo extends Component {
    static propTypes = {
        data: object,
        onChange: func
    };

    calcState(data) {
        return {
            data: fromJS({
                contacts: {
                    title: {
                        russian: 'Контакты',
                        english: 'Contacts'
                    },
                    addresses: []
                }
            }).mergeWith((prev, next) => {
                return next ? next : prev;
            }, data)
        }
    }

    constructor(props) {
        super(props);

        this.state = this.calcState(props.data);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.data) {
            this.setState(this.calcState(nextProps.data));
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.data !== this.state.data;
    }

    render() {
        const d = this.state.data;

        return <section>
            {d.get('additionalText') && this.renderText()}
            {d.get('pageLink') && this.renderPage()}
            {d.get('externalLink') && this.renderLink()}
            {d.get('telephone') && this.renderCall()}
            {this.renderContacts()}
            {this.renderMenu()}
        </section>;
    }

    renderContacts() {
        return this.state.data.getIn(['contacts', 'addresses'])
            .map((address, i) => {
                return <Card key={i}>
                    <FormGroup
                        canDelete={true}
                        onDelete={() => this.handleDeleteAddress(i)}
                        title="Адрес">
                        <LangAddress
                            value={address}
                            onChange={val => this.handleChangeAddress(val, i)}
                        />
                    </FormGroup>
                </Card>;
            });
    }

    renderText() {
        return <Card>
            <FormGroup
                canDelete={true}
                onDelete={() => this.handleFieldDelete('additionalText')}
                title="Дополнительная информация">
                <AdditionalText
                    ref="additionalText"
                    value={this.state.data.get('additionalText')}
                    onChange={val => this.handleFieldChange('additionalText', val)}
                />
            </FormGroup>
        </Card>;
    }

    renderPage() {
        return <Card>
            <FormGroup
                canDelete={true}
                onDelete={() => this.handleFieldDelete('pageLink')}
                title="Переход на экран">
                <AdditionalPage
                    value={this.state.data.get('pageLink')}
                    onChange={val => this.handleFieldChange('pageLink', val)}
                />
            </FormGroup>
        </Card>;
    }

    renderCall() {
        return <Card>
            <FormGroup
                canDelete={true}
                onDelete={() => this.handleFieldDelete('telephone')}
                title="Звонок">
                <AdditionalTelephone
                    value={this.state.data.get('telephone')}
                    onChange={val => this.handleFieldChange('telephone', val)}
                />
            </FormGroup>
        </Card>;
    }

    renderLink() {
        return <Card>
            <FormGroup
                canDelete={true}
                onDelete={() => this.handleFieldDelete('externalLink')}
                title="Внешняя ссылка">
                <AdditionalLink
                    value={this.state.data.get('externalLink')}
                    onChange={val => this.handleFieldChange('externalLink', val)}
                />
            </FormGroup>
        </Card>;
    }

    renderMenu() {
        const d = this.state.data;

        return <IconMenu
            onChange={(ev, val) => this.handleFieldSelect(val)}
            iconButtonElement={<FlatButton label="Добавить поля" />}
            anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
            targetOrigin={{horizontal: 'left', vertical: 'bottom'}}
        >
            {!d.get('additionalText') && <MenuItem value="additionalText" primaryText="Дополнительная информация" />}
            {!d.get('pageLink') && <MenuItem value="pageLink" primaryText="Переход на экран" />}
            {!d.get('externalLink') && <MenuItem value="externalLink" primaryText="Внешняя ссылка" />}
            {!d.get('telephone') && <MenuItem value="telephone" primaryText="Звонок" />}
            {<MenuItem value="contacts" primaryText="Адрес" />}
        </IconMenu>;
    }

    handleChangeAddress(val, i) {
        const data = this.state.data.setIn(['contacts', 'addresses', i], val);
        this.setState({ data });
        this.props.onChange(data);
    }

    handleDeleteAddress(i) {
        const data = this.state.data.mergeIn(['contacts'], {
            addresses: this.state.data
                .getIn(['contacts', 'addresses']).delete(i)
        });

        this.setState({ data });
        this.props.onChange(data);
    }

    handleFieldSelect(field) {
        const data = field === 'contacts'
            ? this.state.data.updateIn(
                ['contacts', 'addresses'],
                list => list.push(Map())
            )
            : this.state.data.set(field, Map());

        this.setState({ data });
    }

    handleFieldChange(field, val) {
        const data = this.state.data.mergeIn([field], val)
        this.props.onChange(data);
    }

    handleFieldDelete(field) {
        const data = this.state.data.set(field, null);
        this.setState({ data });
        this.props.onChange(data);
    }
}
