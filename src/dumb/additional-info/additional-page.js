import React, { Component, PropTypes } from 'react';
import LangInput from 'dumb/lang-input';
import { Map } from 'immutable';
import css from './additional-info.css';
import { getAppPages } from 'provider';
import Dropdown from 'dumb/dropdown';

const { object, func } = PropTypes;

export default class AdditionalPage extends Component {
    static propTypes = {
        value: object,
        onChange: func
    };

    static defaultProps = {
        value: Map()
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({
                value: nextProps.value
            });
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            value: props.value
        };
    }

    render() {
        return (
            <div>
                <div className={css.title}>
                    <LangInput
                        required
                        value={this.state.value.get('title')}
                        placeholder="Заголовок"
                        direction="row"
                        onChange={val => this.handleTitleChange(val)}
                    />
                </div>
                {this.renderPageDropdown()}
            </div>
        );
    }

    renderPageDropdown() {
        return (
            <Dropdown
                label="Страница"
                data={getAppPages()}
                value={this.state.value.get('page') || ''}
                onChange={val => this.handlePageChange(val)}
            />
        );
    }

    handleTitleChange(val) {
        const value = this.state.value.set('title', val);
        this.setState({ value });
        this.props.onChange(value);
    }

    handlePageChange(val) {
        const value = this.state.value.set('page', val);
        this.setState({ value });
        this.props.onChange(value);
    }
}
