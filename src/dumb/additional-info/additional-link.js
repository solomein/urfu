import React, { Component, PropTypes } from 'react';
import LangInput from 'dumb/lang-input';
import { TextField } from 'dumb/text-field';
import { Map } from 'immutable';
import css from './additional-info.css';

const { object, func } = PropTypes;

export default class AdditionalTelephone extends Component {
    static propTypes = {
        value: object,
        onChange: func
    };

    static defaultProps = {
        value: Map()
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({
                value: nextProps.value
            });
        }
    }

    constructor(props) {
        super(props);

        this.state = {
            value: props.value
        };
    }

    render() {
        return (
            <div>
                <div className={css.title}>
                    <LangInput
                        required
                        value={this.state.value.get('title')}
                        placeholder="Заголовок"
                        direction="row"
                        onChange={val => this.handleTitleChange(val)}
                    />
                </div>
                <TextField
                    value={this.state.value.get('url')}
                    placeholder="URL"
                    onChange={val => this.handleUrlChange(val)}
                />
            </div>
        );
    }

    handleTitleChange(val) {
        const value = this.state.value.set('title', val);
        this.setState({ value });
        this.props.onChange(value);
    }

    handleUrlChange(val) {
        const value = this.state.value.set('url', val);
        this.setState({ value });
        this.props.onChange(value);
    }
}
