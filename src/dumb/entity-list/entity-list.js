import React, { Component, PropTypes } from 'react';
import List from 'material-ui/lib/lists/list';
import EntityItem from './entity-item';
/*eslint-disable no-unused-vars */
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
/*eslint-enable no-unused-vars */

const { func, object, bool } = PropTypes;

@DragDropContext(HTML5Backend)
export default class EntityList extends Component {
    static propTypes = {
        onSelect: func.isRequired,
        onDelete: func.isRequired,
        onMove: func.isRequired,
        onMoveEnd: func.isRequired,
        data: object.isRequired,
        readonly: bool,
        dataConverter: func
    };


    render() {
        return <List>
            {this.renderEntities()}
        </List>;
    }

    renderEntities() {
        return this.props.data.map((entity, i) => {
            const d = (this.props.dataConverter || this.dataConverter)(entity);
            return <EntityItem
                key={d.id}
                id={d.id}
                index={i}
                primaryText={d.primaryText}
                secondaryText={d.secondaryText}
                disabled={entity.get('disabled')}
                onSelect={data => this.handleSelect(data, i)}
                onDelete={data => this.handleDelete(data, i)}
                onMove={(drag, hover) => this.handleMove(drag, hover)}
                onMoveEnd={data => this.handleMoveEnd(data)}
                readonly={this.props.readonly}
            />;
        });
    }

    dataConverter(entity) {
        return {
            id: entity.get('id'),
            primaryText: entity.get('primaryText'),
            secondaryText: entity.get('secondaryText')
        };
    }

    handleSelect(data, i) {
        this.props.onSelect(data, i);
    }

    handleDelete(data, i) {
        this.props.onDelete(data, i);
    }

    handleMove(drag, hover) {
        this.props.onMove({
            drag, hover
        });
    }

    handleMoveEnd(data) {
        this.props.onMoveEnd(data);
    }
}
