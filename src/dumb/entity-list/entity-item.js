import React, { Component, PropTypes } from 'react';
import Avatar from 'material-ui/lib/avatar';
import IconButton from 'material-ui/lib/icon-button';
import DeleteIcon from 'material-ui/lib/svg-icons/action/delete';
import DragHandleIcon from 'material-ui/lib/svg-icons/editor/drag-handle';
import { findDOMNode } from 'react-dom';
import css from './entity-item.css';
import { withMods } from 'utils/cssm';
/*eslint-disable no-unused-vars */
import { DragSource, DropTarget } from 'react-dnd';

const mod = withMods(css);
const { func, string, bool } = PropTypes;

const cardSource = {
    beginDrag(props) {
        return {
            id: props.id,
            index: props.index
        };
    },

    endDrag(props) {
        props.onMoveEnd({
            id: props.id,
            index: props.index
        });
    }
};

const cardTarget = {
    hover(props, monitor, component) {
        const dragIndex = monitor.getItem().index;
        const hoverIndex = props.index;

        if (dragIndex === hoverIndex) {
            return;
        }

        const hoverBoundingRect = findDOMNode(component).getBoundingClientRect();
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
        const clientOffset = monitor.getClientOffset();
        const hoverClientY = clientOffset.y - hoverBoundingRect.top;

        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            return;
        }

        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            return;
        }

        props.onMove(dragIndex, hoverIndex);
        monitor.getItem().index = hoverIndex;

    }
};
/*eslint-enable no-unused-vars */

@DropTarget('item', cardTarget, connect => ({
    connectDropTarget: connect.dropTarget()
}))
@DragSource('item', cardSource, (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging()
}))
export default class EntityItem extends Component {
    static propTypes = {
        onSelect: func.isRequired,
        onDelete: func.isRequired,
        id: string.isRequired,
        primaryText: string.isRequired,
        secondaryText: string,
        icon: string,
        disabled: bool,
        readonly: bool,
        isDragging: bool,
        connectDropTarget: func,
        connectDragSource: func,
        connectDragPreview: func
    };

    render() {
        const {
            isDragging,
            connectDropTarget,
            connectDragPreview
        } = this.props;

        return connectDropTarget(
            <div
                className={mod.root({ drag: isDragging })}
                onClick={() => {this.handleSelect()}}
            >
                {connectDragPreview(
                    <div className={css.text}>
                        <div className={css.primary}>
                            {this.props.primaryText}
                        </div>
                        {this.props.secondaryText &&
                            <div className={css.secondary}>
                                {this.props.secondaryText}
                            </div>
                        }
                    </div>
                )}
                <div className={css.bg}></div>
                {this.renderDeleteButton()}
                {this.renderHandle()}
            </div>
       );
    }

    renderIcon() {
        return <div className={css.icon}><Avatar /></div>;
    }

    renderDeleteButton() {
        if (this.props.readonly) {
            return;
        }

        return <div className={css.action}>
            <IconButton
                tooltipPosition="top-left"
                tooltip="Удалить"
                onTouchTap={this.handleDelete.bind(this)}
                disabled={this.props.disabled}
            >
                <DeleteIcon />
            </IconButton>
        </div>;
    }

    renderHandle() {
        if (this.props.readonly) {
            return;
        }

        return this.props.connectDragSource(
            <div className={css.handle}>
                <DragHandleIcon />
            </div>
        )
    }

    handleDelete(ev) {
        ev.stopPropagation()
        this.props.onDelete({
            id: this.props.id
        });
    }

    handleSelect() {
        this.props.onSelect({
            id: this.props.id
        });
    }
}
