import React, { Component, PropTypes } from 'react';
import css from './lang-input.css';
import Flag from 'dumb/flag';
import { Map } from 'immutable';
import { TextField } from 'dumb/text-field';

const { bool, string, object, func } = PropTypes;

export default class LangInput extends Component {
    static propTypes = {
        multiline: bool,
        placeholder: string,
        label: string,
        value: object,
        required: bool,
        onChange: func
    };

    static defaultProps = {
        multiline: false,
        placeholder: '',
        label: '',
        value: Map()
    };

    constructor(props) {
        super(props);
        this.state = {
            invalid: Map({
                russian: false,
                english: false
            }),
            value: props.value
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({
                value: nextProps.value
            });
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        return nextState.value !== this.state.value;
    }

    render() {
        return <section className={css.root}>
            {this.renderInput('ru', 'russian')}
            {this.renderInput('us', 'english')}
        </section>;
    }

    renderInput(country, lang) {
        return <div className={css.lang}>
            <Flag country={country} />
            <div className={css.controls}>
                <TextField
                    value={this.state.value.get(lang)}
                    placeholder={this.props.placeholder}
                    label={this.props.label}
                    multiline={this.props.multiline}
                    onChange={val => this.handleChange(val, lang)}
                    message={this.props.required ? 'Обязательное поле' : ''}
                    invalid={this.state.invalid.get(lang)}
                />
            </div>
        </div>;
    }

    handleChange(val, lang) {
        const value = this.state.value.set(lang, val);

        this.setState({
            value,
            invalid: this.state.invalid.set(
                lang, this.props.required && val.length === 0
            )
        });

        this.props.onChange(value);
    }
}
