import React, { Component, PropTypes } from 'react';
import Category from 'dumb/category';
import { StaggeredMotion, spring } from 'react-motion'
import css from './category-list.css';
import range from 'lodash/range';

const motionConf = { stiffness: 400, damping: 26 };
const { func, object, bool } = PropTypes;

export default class CategoryList extends Component {
    static propTypes = {
        onSelect: func.isRequired,
        data: object,
        animate: bool
    };

    getStyles(prevStyles) {
        return prevStyles.map((style, i) => {
            return i === 0
                ? {
                    opacity: spring(1, motionConf),
                    scale: spring(1, motionConf)
                }
                : {
                    opacity: spring(prevStyles[i - 1].opacity, motionConf),
                    scale: spring(prevStyles[i - 1].scale, motionConf)
                };
        });
    }

    getDefStyles(size) {
        const style = this.props.animate
            ? { opacity: 0, scale: 0 }
            : { opacity: 1, scale: 1 };

        return range(size).map(() => style);
    }

    render() {
        return <section className={css.root}>
            {this.renderCategories()}
        </section>;
    }

    renderCategories() {
        const size = this.props.data.size;
        if (!size) {
            return;
        }

        return <StaggeredMotion
            defaultStyles={this.getDefStyles(size)}
            styles={this.getStyles}
        >
            {styles =>
                <div className={css.container}>
                    {styles.map(({ opacity, scale }, i) => {
                        const category = this.props.data.get(i);
                        const categoryStyle = {
                            opacity,
                            transform: `scale(${scale})`
                        };
                        return <div
                            key={i}
                            className={css.item}
                            style={categoryStyle}
                        >
                            <Category
                                onSelect={this.handleSelect.bind(this)}
                                name={category.getIn(['title', 'russian'])}
                                type={category.get('type')}
                                color={category.get('color')}
                                id={category.get('id')}
                            />
                        </div>
                    })}
                </div>
            }
        </StaggeredMotion>
    }

    handleSelect(data) {
        this.props.onSelect(data);
    }
}
