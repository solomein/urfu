import { category, CategoryType } from 'provider';

export function index() {
    return '/';
}

export function login() {
    return '/login';
}

export function entities(categoryId, subCategoryId) {
    return subCategoryId
        ? `/categories/${categoryId}/sub/${subCategoryId}`
        : `/categories/${categoryId}`;
}

export function newPlace(categoryId, subCategoryId) {
    return subCategoryId
        ? `/categories/${categoryId}/sub/${subCategoryId}/place`
        : `/categories/${categoryId}/place`;
}

export function place(placeId, categoryId, subCategoryId) {
    return subCategoryId
        ? `/categories/${categoryId}/sub/${subCategoryId}/place/${placeId}`
        : `/categories/${categoryId}/place/${placeId}`;
}

export function newCheckList(categoryId) {
    return `/categories/${categoryId}/checkList/new`;
}

export function checkList(checkListId, categoryId) {
    return `/categories/${categoryId}/checkList/${checkListId}`;
}

export function newTask(checkListId, categoryId) {
    return `/categories/${categoryId}/checkList/${checkListId}/task`;
}

export function task(taskIndex, checkListId, categoryId) {
    return `/categories/${categoryId}/checkList/${checkListId}/task/${taskIndex}`;
}

export function newFoodservicePlace(categoryId) {
    return `/categories/${categoryId}/foodservicePlace/new`;
}

export function foodservicePlace(foodservicePlaceId, categoryId) {
    return `/categories/${categoryId}/foodservicePlace/${foodservicePlaceId}`;
}

export function newFoodservice(foodservicePlaceId, categoryId) {
    return `/categories/${categoryId}/foodservicePlace/${foodservicePlaceId}/foodservice`;
}

export function foodservice(foodserviceId, foodservicePlaceId, categoryId) {
    return `/categories/${categoryId}/foodservicePlace/${foodservicePlaceId}/foodservice/${foodserviceId}`;
}

export function newFAQ(categoryId) {
    return `/categories/${categoryId}/FAQ`;
}

export function FAQ(FAQId, categoryId) {
    return `/categories/${categoryId}/FAQ/${FAQId}`;
}

export function newCampusBuilding(categoryId) {
    return `/categories/${categoryId}/campusBuilding`;
}

export function campusBuilding(campusBuildingId, categoryId) {
    return `/categories/${categoryId}/campusBuilding/${campusBuildingId}`;
}

export function entity(entityId, categoryId, subCategoryId) {
    return category.getCategoryType(categoryId).then(type => {
        if (type === CategoryType.CHECK_LIST) {
            return checkList(entityId, categoryId);
        }

        if (type === CategoryType.FAQ) {
            return FAQ(entityId, categoryId);
        }

        if (type === CategoryType.CANTEENS) {
            return foodservicePlace(entityId, categoryId);
        }

        if (type === CategoryType.CAMPUS_MAP) {
            return campusBuilding(entityId, categoryId);
        }

        return place(entityId, categoryId, subCategoryId);
    });
}

export function newEntity(categoryId, subCategoryId) {
    return category.getCategoryType(categoryId).then(type => {
        if (type === CategoryType.CHECK_LIST) {
            return newCheckList(categoryId);
        }

        if (type === CategoryType.FAQ) {
            return newFAQ(categoryId);
        }

        if (type === CategoryType.CANTEENS) {
            return newFoodservicePlace(categoryId);
        }

        if (type === CategoryType.CAMPUS_MAP) {
            return newCampusBuilding(categoryId);
        }

        return newPlace(categoryId, subCategoryId);
    });
}
