import * as Types from './action-types';
import { category } from 'provider';

function getCategoryRequest() {
    return {
        type: Types.GET_CATEGORY_REQUEST
    };
}

function getCategorySuccess(response) {
    return {
        type: Types.GET_CATEGORY_SUCCESS,
        response
    };
}

function getCategoryFailure(error) {
    return {
        type: Types.GET_CATEGORY_FAILURE,
        error
    }
}

export function getCategory(id) {
    return (dispatch) => {
        dispatch(getCategoryRequest(id));
        category.getCategory(id).then(
            data => getCategorySuccess(data),
            error => getCategoryFailure(error)
        );
    };
}
