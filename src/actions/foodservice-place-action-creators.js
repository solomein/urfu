import * as Types from './action-types';
import { foodservicePlace } from 'provider';

function getFoodservicePlaceRequest() {
    return {
        type: Types.GET_FOODSERVICE_PLACE_REQUEST
    };
}

function getFoodservicePlaceSuccess(response) {
    return {
        type: Types.GET_FOODSERVICE_PLACE_SUCCESS,
        response
    };
}

function getFoodservicePlaceFailure(error) {
    return {
        type: Types.GET_FOODSERVICE_PLACE_FAILURE,
        error
    };
}

function sendFoodservicePlaceRequest() {
    return {
        type: Types.SEND_FOODSERVICE_PLACE_REQUEST
    };
}

function sendFoodservicePlaceSuccess() {
    return {
        type: Types.SEND_FOODSERVICE_PLACE_SUCCESS
    };
}

function sendFoodservicePlaceFailure(error) {
    return {
        type: Types.SEND_FOODSERVICE_PLACE_FAILURE,
        error
    };
}

export function getFoodservicePlace(id) {
    return (dispatch) => {
        dispatch(getFoodservicePlaceRequest());

        if (!id) {
            dispatch(getFoodservicePlaceSuccess({
                foodservices: []
            }));
        } else {
            foodservicePlace.getEntity(id)
                .then(data => dispatch(getFoodservicePlaceSuccess(data)))
                .catch(error => dispatch(getFoodservicePlaceFailure(error)));
        }
    };
}

export function resetFoodservicePlace() {
    return {
        type: Types.RESET_FOODSERVICE_PLACE
    };
}

export function createFoodservicePlace(data) {
    return dispatch => {
        dispatch(sendFoodservicePlaceRequest())
        foodservicePlace.createEntity(data)
            .then(() => dispatch(sendFoodservicePlaceSuccess()))
            .catch(error => dispatch(sendFoodservicePlaceFailure(error)));
    }
}

export function updateFoodservicePlace(data) {
    return dispatch => {
        dispatch(sendFoodservicePlaceRequest())
        foodservicePlace.updateEntity(data)
            .then(() => dispatch(sendFoodservicePlaceSuccess()))
            .catch(error => dispatch(sendFoodservicePlaceFailure(error)));
    }
}

export function selectFoodservice(index) {
    return {
        type: Types.SELECT_FOODSERVICE,
        index
    };
}

export function updateFoodservice(foodservice, index) {
    return {
        type: Types.UPDATE_FOODSERVICE,
        foodservice,
        index
    };
}

export function addFoodservice(foodservice) {
    return {
        type: Types.ADD_FOODSERVICE,
        foodservice
    };
}

export function deleteFoodservice(index) {
    return {
        type: Types.DELETE_FOODSERVICE,
        index
    };
}

export function moveFoodservice(drag, hover) {
    return {
        type: Types.MOVE_FOODSERVICE,
        drag,
        hover
    }
}

export function persist(foodservicePlace) {
    return {
        type: Types.PERSIST_FOODSERVICE_PLACE,
        foodservicePlace
    };
}

export function halt() {
    return {
        type: Types.HALT_FOODSERVICE_PLACE
    };
}
