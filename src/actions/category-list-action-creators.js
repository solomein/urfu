import * as Types from './action-types';
import { category } from 'provider';

function getCategoryListRequest() {
    return {
        type: Types.GET_CATEGORY_LIST_REQUEST
    };
}

function getCategoryListSuccess(response) {
    return {
        type: Types.GET_CATEGORY_LIST_SUCCESS,
        response
    };
}

function getCategoryListFailure(error) {
    return {
        type: Types.GET_CATEGORY_LIST_FAILURE,
        error
    }
}

export function getCategoryList() {
    return (dispatch) => {
        dispatch(getCategoryListRequest());

        category.getCategoryList()
            .then(data => dispatch(getCategoryListSuccess(data)))
            .catch(error => dispatch(getCategoryListFailure(error)));
    }
}
