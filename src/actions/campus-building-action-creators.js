import * as Types from './action-types';
import { campusBuilding } from 'provider';

function getCampusBuildingRequest() {
    return {
        type: Types.GET_CAMPUS_BUILDING_REQUEST
    };
}

function getCampusBuildingSuccess(response) {
    return {
        type: Types.GET_CAMPUS_BUILDING_SUCCESS,
        response
    };
}

function getCampusBuildingFailure(error) {
    return {
        type: Types.GET_CAMPUS_BUILDING_FAILURE,
        error
    }
}

function sendCampusBuildingRequest() {
    return {
        type: Types.SEND_CAMPUS_BUILDING_REQUEST
    };
}

function sendCampusBuildingSuccess() {
    return {
        type: Types.SEND_CAMPUS_BUILDING_SUCCESS
    };
}

function sendCampusBuildingFailure(error) {
    return {
        type: Types.SEND_CAMPUS_BUILDING_FAILURE,
        error
    };
}

export function resetCampusBuilding() {
    return {
        type: Types.RESET_CAMPUS_BUILDING
    };
}

export function getCampusBuilding(id) {
    return (dispatch) => {
        dispatch(getCampusBuildingRequest());

        if (!id) {
            dispatch(getCampusBuildingSuccess({}));
        } else {
            campusBuilding.getEntity(id)
                .then(data => dispatch(getCampusBuildingSuccess(data)))
                .catch(error => dispatch(getCampusBuildingFailure(error)));
        }
    }
}

export function createCampusBuilding(data) {
    return dispatch => {
        dispatch(sendCampusBuildingRequest())
        campusBuilding.createEntity(data)
            .then(() => dispatch(sendCampusBuildingSuccess()))
            .catch(error => dispatch(sendCampusBuildingFailure(error)));
    }
}

export function updateCampusBuilding(data) {
    return dispatch => {
        dispatch(sendCampusBuildingRequest())
        campusBuilding.updateEntity(data)
            .then(() => dispatch(sendCampusBuildingSuccess()))
            .catch(error => dispatch(sendCampusBuildingFailure(error)));
    }
}
