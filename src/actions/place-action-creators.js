import * as Types from './action-types';
import { place } from 'provider';
import { categoryEntity } from 'provider';

function getPlaceRequest() {
    return {
        type: Types.GET_PLACE_REQUEST
    };
}

function getPlaceSuccess(response, colorConfig) {
    return {
        type: Types.GET_PLACE_SUCCESS,
        canEditColor: colorConfig.canEditColor,
        canEditGradient: colorConfig.canEditGradient,
        response
    };
}

function getPlaceFailure(error) {
    return {
        type: Types.GET_PLACE_FAILURE,
        error
    }
}

function sendPlaceRequest() {
    return {
        type: Types.SEND_PLACE_REQUEST
    };
}

function sendPlaceSuccess() {
    return {
        type: Types.SEND_PLACE_SUCCESS
    };
}

function sendPlaceFailure(error) {
    return {
        type: Types.SEND_PLACE_FAILURE,
        error
    };
}

export function resetPlace() {
    return {
        type: Types.RESET_PLACE
    };
}

export function getPlace(id, categoryId, subCategoryId) {
    return dispatch => {
        dispatch(getPlaceRequest());

        categoryEntity.getColorEditConfig(categoryId).then(config => {
            if (!id) {
                dispatch(getPlaceSuccess({ categoryId, subCategoryId }, config));
            } else {
                place.getEntity(id)
                    .then(data => dispatch(getPlaceSuccess(data, config)))
                    .catch(error => dispatch(getPlaceFailure(error)));
            }
        })

    }
}

export function createPlace(data) {
    return dispatch => {
        dispatch(sendPlaceRequest())
        place.createEntity(data)
            .then(() => dispatch(sendPlaceSuccess()))
            .catch(error => dispatch(sendPlaceFailure(error)));
    }
}

export function updatePlace(data) {
    return dispatch => {
        dispatch(sendPlaceRequest())
        place.updateEntity(data)
            .then(() => dispatch(sendPlaceSuccess()))
            .catch(error => dispatch(sendPlaceFailure(error)));
    }
}
