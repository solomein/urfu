import * as Types from './action-types';
import { faq } from 'provider';

function getFaqRequest() {
    return {
        type: Types.GET_FAQ_REQUEST
    };
}

function getFaqSuccess(response) {
    return {
        type: Types.GET_FAQ_SUCCESS,
        response
    };
}

function getFaqFailure(error) {
    return {
        type: Types.GET_FAQ_FAILURE,
        error
    }
}

function sendFaqRequest() {
    return {
        type: Types.SEND_FAQ_REQUEST
    };
}

function sendFaqSuccess() {
    return {
        type: Types.SEND_FAQ_SUCCESS
    };
}

function sendFaqFailure(error) {
    return {
        type: Types.SEND_FAQ_FAILURE,
        error
    }
}

export function getFaq(id) {
    return (dispatch) => {
        dispatch(getFaqRequest());

        if (!id) {
            dispatch(getFaqSuccess({}));
        } else {
            faq.getEntity(id)
                .then(data => dispatch(getFaqSuccess(data)))
                .catch(error => dispatch(getFaqFailure(error)));
        }
    }
}

export function resetFaq() {
    return {
        type: Types.RESET_FAQ
    };
}

export function createFaq(data) {
    return dispatch => {
        dispatch(sendFaqRequest())
        faq.createEntity(data)
            .then(() => dispatch(sendFaqSuccess()))
            .catch(error => dispatch(sendFaqFailure(error)));
    }
}

export function updateFaq(data) {
    return dispatch => {
        dispatch(sendFaqRequest())
        faq.updateEntity(data)
            .then(() => dispatch(sendFaqSuccess()))
            .catch(error => dispatch(sendFaqFailure(error)));
    }
}
