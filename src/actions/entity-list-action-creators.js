import * as Types from './action-types';

import { categoryEntity } from 'provider';

function getEntityListRequest() {
    return {
        type: Types.GET_ENTITY_LIST_REQUEST
    };
}

function getEntityListSuccess(raw, response, readonly, maxItems) {
    return {
        type: Types.GET_ENTITY_LIST_SUCCESS,
        response,
        raw,
        readonly,
        maxItems
    };
}

function getEntityListFailure(error) {
    return {
        type: Types.GET_ENTITY_LIST_FAILURE,
        error
    };
}

export function getEntityList(categoryId) {
    return (dispatch) => {
        dispatch(getEntityListRequest());

        let data = [];

        categoryEntity.getEntityList(categoryId)
            .then(resp => {
                data = resp;
                return Promise.all([
                    categoryEntity.convertToListItems(categoryId, data),
                    categoryEntity.isReadOnlyEntityList(categoryId),
                    categoryEntity.maxItems(categoryId)
                ]);
            })
            .then(values => {
                dispatch(getEntityListSuccess(
                    data,
                    values[0],
                    values[1],
                    values[2]
                ));
            })
            .catch(error => dispatch(getEntityListFailure(error)));
    };
}

export function resetEntityList() {
    return {
        type: Types.RESET_ENTITY_LIST
    };
}

function deleteEntityListItemRequest(id) {
    return {
        type: Types.DELETE_ENTITY_LIST_ITEM_REQUEST,
        id
    };
}

function deleteEntityListItemSuccess(id) {
    return {
        type: Types.DELETE_ENTITY_LIST_ITEM_SUCCESS,
        id
    };
}

function deleteEntityListItemFailure(error) {
    return {
        type: Types.DELETE_ENTITY_LIST_ITEM_FAILURE,
        error
    };
}

export function deleteEntityListItem(id, categoryId) {
    return (dispatch) => {
        dispatch(deleteEntityListItemRequest(id));
        categoryEntity.deleteEntity(id, categoryId).then(() => {
            dispatch(deleteEntityListItemSuccess(id));
        });
    };
}

export function updateEntityListItem(data, categoryId) {
    return dispatch => {
        categoryEntity.updateEntity(data, categoryId);
    };
}

export function moveEntityListItem(drag, hover) {
    return {
        type: Types.MOVE_ENTITY_LIST_ITEM,
        drag,
        hover
    };
}
