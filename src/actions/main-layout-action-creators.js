import * as Types from './action-types';
import { category } from 'provider';
import { categoryToTitle } from 'helpers/converters';

export function setPrevPath(path) {
    return {
        type: Types.SET_PREV_PATH,
        path
    };
}

export function setTitle(title) {
    return {
        type: Types.SET_TITLE,
        title
    };
}

export function setTitleAsCategory(categoryId) {
    return dispatch => {
        category.getCategory(categoryId)
            .then(category => dispatch(setTitle(categoryToTitle(category))));
    };
}

export function showMessage(message) {
    return {
        type: Types.SHOW_MESSAGE,
        message: {
            text: message,
            stamp: Date.now()
        }
    };
}

export function removeMessage(stamp) {
    return {
        type: Types.REMOVE_MESSAGE,
        stamp
    };
}
