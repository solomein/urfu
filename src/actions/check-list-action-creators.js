import * as Types from './action-types';
import { checkList } from 'provider';

function getCheckListRequest() {
    return {
        type: Types.GET_CHECK_LIST_REQUEST
    };
}

function getCheckListSuccess(response) {
    return {
        type: Types.GET_CHECK_LIST_SUCCESS,
        response
    };
}

function getCheckListFailure(error) {
    return {
        type: Types.GET_CHECK_LIST_FAILURE,
        error
    };
}

function sendCheckListRequest() {
    return {
        type: Types.SEND_CHECK_LIST_REQUEST
    };
}

function sendCheckListSuccess() {
    return {
        type: Types.SEND_CHECK_LIST_SUCCESS
    };
}

function sendCheckListFailure(error) {
    return {
        type: Types.SEND_CHECK_LIST_FAILURE,
        error
    };
}

export function getCheckList(id) {
    return (dispatch) => {
        dispatch(getCheckListRequest());

        if (!id) {
            dispatch(getCheckListSuccess({
                tasks: []
            }));
        } else {
            checkList.getEntity(id)
                .then(data => dispatch(getCheckListSuccess(data)))
                .catch(error => dispatch(getCheckListFailure(error)));
        }
    };
}

export function resetCheckList() {
    return {
        type: Types.RESET_CHECK_LIST
    };
}

export function createCheckList(data) {
    return dispatch => {
        dispatch(sendCheckListRequest());
        checkList.createEntity(data)
            .then(() => dispatch(sendCheckListSuccess()))
            .catch(error => dispatch(sendCheckListFailure(error)));
    }
}

export function updateCheckList(data) {
    return dispatch => {
        dispatch(sendCheckListRequest())
        checkList.updateEntity(data)
            .then(() => dispatch(sendCheckListSuccess()))
            .catch(error => dispatch(sendCheckListFailure(error)));
    }
}

export function selectTask(index) {
    return {
        type: Types.SELECT_TASK,
        index
    };
}

export function updateTask(task, index) {
    return {
        type: Types.UPDATE_TASK,
        task,
        index
    };
}

export function addTask(task) {
    return {
        type: Types.ADD_TASK,
        task
    };
}

export function deleteTask(index) {
    return {
        type: Types.DELETE_TASK,
        index
    };
}

export function moveTask(drag, hover) {
    return {
        type: Types.MOVE_TASK,
        drag,
        hover
    }
}

export function persist(checkList) {
    return {
        type: Types.PERSIST_CHECK_LIST,
        checkList
    };
}

export function halt() {
    return {
        type: Types.HALT_CHECK_LIST
    };
}
