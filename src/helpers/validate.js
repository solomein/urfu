export function langTextIsNotEmpty(value) {
    if (!value) {
        return false;
    }

    const russian = value.get('russian');
    const english = value.get('english');

    return russian && russian.length > 0
        && english && english.length > 0;
}

export function addressIsValid(address) {
    if (!address) {
        return false;
    }

    const title = address.get('title');

    return langTextIsNotEmpty(title);
}
