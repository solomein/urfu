import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { logout } from 'auth';
import AppBar from 'dumb/app-bar';
import css from './main-layout.css';
import { login } from 'nav';
import RawTheme from 'theme';
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import { removeMessage } from 'actions/main-layout-action-creators';

import Snackbar from 'dumb/snackbar';

const { object, element, func } = PropTypes;

class MainLayout extends Component {
    static contextTypes = {
        router: object
    };

    static childContextTypes = {
        muiTheme: object
    };

    static propTypes = {
        mainLayout: object,
        children: element,
        dispatch: func
    };

    getChildContext() {
        return {
            muiTheme: ThemeManager.getMuiTheme(RawTheme)
        };
    }

    constructor(props) {
        super(props);
    }

    render() {
        const layout = this.props.mainLayout;

        return (
            <section className={css.root}>
                <div className={css.bar}>
                    <AppBar
                        title={layout.get('title')}
                        showBack={Boolean(layout.get('prevPath'))}
                        onLogout={() => this.handleLogout()}
                        onBack={() => this.handleBack()}
                    />
                </div>
                <div className={css.content}>
                    {this.props.children}
                </div>
                {this.renderSnackbar()}
            </section>
        );
    }

    renderSnackbar() {
        const message = this.props.mainLayout.getIn(['messages', 0]);
        const text = message && message.text;
        const stamp = message && message.stamp;

        return <Snackbar
            open={Boolean(text)}
            message={text}
            onClose={() => this.removeMessage(stamp)}
        />;
    }

    removeMessage(stamp) {
        this.props.dispatch(removeMessage(stamp));
    }

    handleLogout() {
        logout().then(() => {
            this.context.router.replace(login());
        });
    }

    handleBack() {
        this.context.router.push(this.props.mainLayout.get('prevPath'));
    }
}

export default connect(state => {
    return {
        mainLayout: state.get('mainLayout')
    };
})(MainLayout);
