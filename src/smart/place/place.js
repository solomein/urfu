import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { setTitleAsCategory, setPrevPath, showMessage } from 'actions/main-layout-action-creators';
import { getPlace, updatePlace, createPlace, resetPlace } from 'actions/place-action-creators';
import { entities } from 'nav';
import Place from 'dumb/place';
import FallMotion from 'dumb/fall-motion';

const { object, func } = PropTypes;

class SmartPlace extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        dispatch: func,
        params: object,
        place: object
    };

    componentWillMount() {
        const { dispatch, params } = this.props;

        dispatch(setTitleAsCategory(params.categoryId));
        dispatch(setPrevPath(entities(params.categoryId)));
        dispatch(getPlace(params.placeId, params.categoryId, params.subCategoryId));
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.place !== this.props.place;
    }

    componentWillReceiveProps(nextProps) {
        const error = nextProps.place.get('error');
        if (error) {
            this.props.dispatch(showMessage(error));
        }
        if (nextProps.place.get('sended')) {
            this.props.dispatch(showMessage('Место успешно сохранено'));
            this.context.router.push(entities(this.props.params.categoryId));
        }
    }

    componentWillUnmount() {
        this.props.dispatch(resetPlace());
    }

    render() {
        const place = this.props.place;
        const data = place.get('data');

        if (place.get('fetching') || data === null) {
            return null;
        }

        return <FallMotion>
            <Place
                data={data}
                canEditColor={place.get('canEditColor')}
                canEditGradient={place.get('canEditGradient')}
                onSave={data => this.handleSave(data)}
            />
        </FallMotion>;
    }

    handleSave(data) {
        const id = data.get('id');
        const { dispatch } = this.props;

        if (id) {
            dispatch(updatePlace(data.toJS()));
        }
        else {
            dispatch(createPlace(data.toJS()));
        }
    }
}

export default connect(state => {
    return {
        place: state.get('place')
    }
})(SmartPlace);
