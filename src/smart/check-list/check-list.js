import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { setTitle, setPrevPath, showMessage } from 'actions/main-layout-action-creators';
import {
    getCheckList,
    createCheckList,
    updateCheckList,
    resetCheckList,
    persist,
    halt,
    deleteTask,
    moveTask
} from 'actions/check-list-action-creators';
import { entities, task, newTask } from 'nav';
import CheckList from 'dumb/check-list';
import FallMotion from 'dumb/fall-motion';

const { object, func } = PropTypes;

class SmartCheckList extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        dispatch: func,
        params: object,
        checkList: object
    };

    componentWillMount() {
        const { dispatch, params } = this.props;

        dispatch(setTitle('Инструкция'));
        dispatch(setPrevPath(entities(params.categoryId)));
        if (this.props.checkList.get('persist')) {
            dispatch(halt());
        }
        else {
            dispatch(getCheckList(params.checkListId));
        }
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.checkList !== this.props.checkList;
    }

    componentWillReceiveProps(nextProps) {
        const error = nextProps.checkList.get('error')
        if (error) {
            this.props.dispatch(showMessage(error));
        }
        if (nextProps.checkList.get('sended')) {
            this.props.dispatch(showMessage('Инструкция успешно сохранена'));
            this.context.router.push(entities(this.props.params.categoryId));
        }
    }

    componentWillUnmount() {
        this.props.dispatch(resetCheckList());
    }

    render() {
        const list = this.props.checkList;
        const data = list.get('data')

        if (list.get('fetching') || data === null) {
            return null;
        }

        return <FallMotion>
            <CheckList
                ref="checkList"
                data={data}
                onSave={data => this.handleSave(data)}
                onTaskSelect={i => this.handleTaskSelect(i)}
                onTaskDelete={i => this.handleTaskDelete(i)}
                onTaskAdd={() => this.handleTaskAdd()}
                onTaskMove={data => this.handleTaskMove(data)}
            />
        </FallMotion>;
    }

    handleTaskSelect(i) {
        this.props.dispatch(persist(this.refs.checkList.getData()));
        const { categoryId, checkListId } = this.props.params;
        this.context.router.push(task(i, checkListId || 'new', categoryId));
    }

    handleTaskDelete(i) {
        this.props.dispatch(deleteTask(i));
    }

    handleTaskAdd() {
        this.props.dispatch(persist(this.refs.checkList.getData()));
        const { categoryId, checkListId } = this.props.params;
        this.context.router.push(newTask(checkListId || 'new', categoryId));
    }

    handleTaskMove(data) {
        this.props.dispatch(moveTask(data.drag, data.hover));
    }

    handleSave(data) {
        const id = data.get('id');
        const { dispatch } = this.props;

        if (id) {
            dispatch(updateCheckList(data.toJS()));
        }
        else {
            dispatch(createCheckList(data.toJS()));
        }
    }
}

export default connect(state => {
    return {
        checkList: state.get('checkList')
    }
})(SmartCheckList);
