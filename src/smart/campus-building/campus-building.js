import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
    setTitle,
    setPrevPath,
    showMessage
} from 'actions/main-layout-action-creators';
import {
    getCampusBuilding,
    updateCampusBuilding,
    createCampusBuilding,
    resetCampusBuilding
} from 'actions/campus-building-action-creators';
import { entities } from 'nav';
import CampusBuilding from 'dumb/campus-building';
import FallMotion from 'dumb/fall-motion';

const { object, func } = PropTypes;

class SmartCampusBuilding extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        dispatch: func,
        params: object,
        campusBuilding: object
    };

    componentWillMount() {
        const { dispatch, params } = this.props;

        dispatch(setTitle('Здание'));
        dispatch(setPrevPath(entities(params.categoryId)));
        dispatch(getCampusBuilding(params.campusBuildingId));
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.campusBuilding !== this.props.campusBuilding;
    }

    componentWillReceiveProps(nextProps) {
        const error = nextProps.campusBuilding.get('error')
        if (error) {
            this.props.dispatch(showMessage(error));
        }
        if (nextProps.campusBuilding.get('sended')) {
            this.props.dispatch(showMessage('Здание успешно сохранено'));
            this.context.router.push(entities(this.props.params.categoryId));
        }
    }

    componentWillUnmount() {
        this.props.dispatch(resetCampusBuilding());
    }

    render() {
        const campus = this.props.campusBuilding;
        const data = campus.get('data');

        if (campus.get('fetching') || data === null) {
            return null;
        }

        return <FallMotion>
            <CampusBuilding
                data={data}
                onSave={data => this.handleSave(data)}
                onError={error => this.handleError(error)}
            />
        </FallMotion>;
    }

    handleSave(data) {
        const id = data.get('id');
        const { dispatch } = this.props;

        if (id) {
            dispatch(updateCampusBuilding(data.toJS()));
        }
        else {
            dispatch(createCampusBuilding(data.toJS()));
        }
    }

    handleError(error) {
        this.props.dispatch(showMessage(error));
    }
}

export default connect(state => {
    return {
        campusBuilding: state.get('campusBuilding')
    }
})(SmartCampusBuilding);
