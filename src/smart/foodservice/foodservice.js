import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { setTitle, setPrevPath } from 'actions/main-layout-action-creators';
import {
    selectFoodservice,
    updateFoodservice,
    addFoodservice
} from 'actions/foodservice-place-action-creators';
import { foodservicePlace } from 'nav';
import { Foodservice } from 'dumb/foodservice-place';
import uniqueId from 'lodash/uniqueId';
import FallMotion from 'dumb/fall-motion';

const { object, func } = PropTypes;

class SmartFoodservice extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static propTypes = {
        dispatch: func,
        params: object,
        foodservicePlace: object
    };

    componentWillMount() {
        const { dispatch, params } = this.props;

        dispatch(setTitle('Заведение'));
        dispatch(setPrevPath(foodservicePlace(params.foodservicePlaceId, params.categoryId)));
        dispatch(selectFoodservice(params.foodserviceIndex));
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.foodservicePlace.get('foodservice') === null) {
            const { foodservicePlaceId, categoryId }  = this.props.params;
            this.context.router.push(foodservicePlace(foodservicePlaceId, categoryId))
        }
    }

    render() {
        return <FallMotion>
            <Foodservice
                data={this.props.foodservicePlace.get('foodservice')}
                onSave={data => this.handleSave(data)}
            />
        </FallMotion>;
    }

    handleSave(data) {
        const index = this.props.params.foodserviceIndex;
        const { foodservicePlaceId, categoryId }  = this.props.params;

        if (index) {
            this.props.dispatch(updateFoodservice(data, index));
        }
        else {
            this.props.dispatch(addFoodservice(data.set('_cid', uniqueId())));
        }

        this.context.router.push(foodservicePlace(foodservicePlaceId, categoryId));
    }
}

export default connect(state => {
    return {
        foodservicePlace: state.get('foodservicePlace')
    }
})(SmartFoodservice);
