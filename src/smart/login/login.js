import React, { Component, PropTypes } from 'react';
import LoginForm from 'dumb/login-form';
import { login } from 'auth';
import css from './login.css';
import RawTheme from 'theme';
import ThemeManager from 'material-ui/lib/styles/theme-manager';
import Snackbar from 'dumb/snackbar';

const { object } = PropTypes;

export default class Login extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        location: object
    };

    static childContextTypes = {
        muiTheme: object
    };

    getChildContext() {
        return {
            muiTheme: ThemeManager.getMuiTheme(RawTheme)
        };
    }

    constructor(props) {
        super(props);
        this.state = { error: false };
    }

    render() {
        return (
            <section className={css.root}>
                <LoginForm onSubmit={this.handleSubmit.bind(this)} />
                <Snackbar
                    open={this.state.error}
                    message="Ошибка входа"
                />
            </section>
        );
    }

    handleSubmit(name, password) {
        // const { location } = this.props;
        // const { router } = this.context;

        login(name, password).then(() => {
            // if (location.state && location.state.nextPathname) {
            //     router.replace(location.state.nextPathname);
            // }
            // else {
            //     router.replace('/');
            // }
        }).then(() => {
            window.location.reload();
        }).catch(() => {
            this.setState({ error: true });
        });
    }
}
