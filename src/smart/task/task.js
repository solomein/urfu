import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { setTitle, setPrevPath } from 'actions/main-layout-action-creators';
import { selectTask, updateTask, addTask } from 'actions/check-list-action-creators';
import { checkList } from 'nav';
import { Task } from 'dumb/check-list';
import uniqueId from 'lodash/uniqueId';
import FallMotion from 'dumb/fall-motion';

const { object, func } = PropTypes;

class SmartCheckList extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        dispatch: func,
        params: object,
        checkList: object
    };

    componentWillMount() {
        const { dispatch, params } = this.props;

        dispatch(setTitle('Шаг инструкции'));
        dispatch(setPrevPath(checkList(params.checkListId, params.categoryId)));
        dispatch(selectTask(params.taskIndex));
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.checkList.get('task') === null) {
            const { checkListId, categoryId }  = this.props.params;
            this.context.router.push(checkList(checkListId, categoryId))
        }
    }

    render() {
        return <FallMotion>
            <Task
                data={this.props.checkList.get('task')}
                onSave={data => this.handleSave(data)}
            />
        </FallMotion>;
    }

    handleSave(data) {
        const index = this.props.params.taskIndex;
        const { checkListId, categoryId }  = this.props.params;

        if (index) {
            this.props.dispatch(updateTask(data, index));
        }
        else {
            this.props.dispatch(addTask(data.set('_cid', uniqueId())));
        }

        this.context.router.push(checkList(checkListId, categoryId));
    }
}

export default connect(state => {
    return {
        checkList: state.get('checkList')
    }
})(SmartCheckList);
