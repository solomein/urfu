import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
    getEntityList,
    deleteEntityListItem,
    updateEntityListItem,
    moveEntityListItem,
    resetEntityList
} from 'actions/entity-list-action-creators';
import {
    setTitleAsCategory,
    setPrevPath,
    showMessage
} from 'actions/main-layout-action-creators';
import FloatingActionButton from 'material-ui/lib/floating-action-button';
import ContentAdd from 'material-ui/lib/svg-icons/content/add';
import EntityList from 'dumb/entity-list';
import ListContainer from 'dumb/list-container';
import { newEntity, entity, index } from 'nav';
import FallMotion from 'dumb/fall-motion';

import css from './entities.css';

const { object, func } = PropTypes;

class Entities extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        dispatch: func,
        params: object,
        entityList: object
    };

    componentWillMount() {
        this.props.dispatch(getEntityList(this.props.params.categoryId));
        this.props.dispatch(setTitleAsCategory(this.props.params.categoryId));
        this.props.dispatch(setPrevPath(index()));
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.entityList !== this.props.entityList;
    }

    componentWillUnmount() {
        this.props.dispatch(resetEntityList());
    }

    componentWillReceiveProps(nextProps) {
        const error = nextProps.entityList.get('error');
        if (error) {
            this.props.dispatch(showMessage(error));
        }
    }

    render() {
        const list = this.props.entityList;

        if (list.get('fetching') || list.get('items') === null) {
            return null;
        }

        return (
            <FallMotion>
                {list.get('items').size ? this.renderList() : this.renderEmpty()}
                {this.renderFloatingButton()}
            </FallMotion>
        );
    }

    renderList() {
        return <ListContainer>
            <EntityList
                readonly={this.props.entityList.get('readonly')}
                data={this.props.entityList.get('items')}
                onSelect={data => this.handleSelectEntity(data)}
                onDelete={data => this.handleDeleteEntity(data)}
                onMove={data => this.handleMoveEntity(data)}
                onMoveEnd={data => this.handleMoveEndEntity(data)}
            />
        </ListContainer>
    }

    renderEmpty() {
        return <div className={css.empty}>
            Здесь пока ничего нет
        </div>;
    }

    renderFloatingButton() {
        const list = this.props.entityList;
        const disable = list.get('readonly')
            || list.get('maxItems') <= list.get('items').size;

        if (disable) {
            return;
        }

        return <div className={css.addButton}>
            <FloatingActionButton
                primary={true}
                onTouchTap={() => this.handleAddClick()}
            >
                <ContentAdd />
            </FloatingActionButton>
        </div>;
    }

    handleSelectEntity(data) {
        entity(data.id, this.props.params.categoryId)
            .then(path => this.context.router.push(path));
    }

    handleDeleteEntity(data) {
        this.props.dispatch(deleteEntityListItem(
            data.id,
            this.props.params.categoryId
        ));
    }

    handleMoveEntity(data) {
        this.props.dispatch(moveEntityListItem(data.drag, data.hover));
    }

    handleMoveEndEntity(data) {
        const entity = this.props.entityList.get('raw').find(item => {
            return item.get('id') === data.id;
        });
        this.props.dispatch(updateEntityListItem(
            entity.set('orderNumber', data.index).toJS(),
            this.props.params.categoryId
        ));
    }

    handleAddClick() {
        newEntity(this.props.params.categoryId)
            .then(path => this.context.router.push(path));
    }
}

export default connect(state => {
    return {
        entityList: state.get('entityList')
    }
})(Entities);
