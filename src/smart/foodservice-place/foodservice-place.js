import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
    setTitle,
    setPrevPath,
    showMessage
} from 'actions/main-layout-action-creators';
import {
    getFoodservicePlace,
    updateFoodservicePlace,
    createFoodservicePlace,
    resetFoodservicePlace,
    persist,
    halt,
    deleteFoodservice,
    moveFoodservice
} from 'actions/foodservice-place-action-creators';
import { entities, foodservice, newFoodservice } from 'nav';
import FoodservicePlace from 'dumb/foodservice-place';
import FallMotion from 'dumb/fall-motion';

const { object, func } = PropTypes;

class SmartFoodservicePlace extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    static propTypes = {
        dispatch: func,
        params: object,
        foodservicePlace: object
    };

    componentWillMount() {
        const { dispatch, params } = this.props;

        dispatch(setTitle('Место общественного питания'));
        dispatch(setPrevPath(entities(params.categoryId)));
        if (this.props.foodservicePlace.get('persist')) {
            dispatch(halt());
        }
        else {
            dispatch(getFoodservicePlace(params.foodservicePlaceId));
        }
    }

    componentWillReceiveProps(nextProps) {
        const error = nextProps.foodservicePlace.get('error');
        if (error) {
            this.props.dispatch(showMessage(error));
        }
        if (nextProps.foodservicePlace.get('sended')) {
            this.props.dispatch(showMessage('Место успешно сохранено'));
            this.context.router.push(entities(this.props.params.categoryId));
        }
    }

    componentWillUnmount() {
        this.props.dispatch(resetFoodservicePlace());
    }

    render() {
        const place = this.props.foodservicePlace;
        const data = place.get('data');

        if (place.get('fetching') || data === null) {
            return null;
        }

        return <FallMotion>
            <FoodservicePlace
                ref="foodservicePlace"
                data={data}
                onSave={data => this.handleSave(data)}
                onFoodserviceSelect={i => this.handleFoodserviceSelect(i)}
                onFoodserviceDelete={i => this.handleFoodserviceDelete(i)}
                onFoodserviceAdd={() => this.handleFoodserviceAdd()}
                onFoodserviceMove={data => this.handleFoodserviceMove(data)}
            />
        </FallMotion>;
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.foodservicePlace !== this.props.foodservicePlace
    }

    handleFoodserviceSelect(i) {
        this.props.dispatch(persist(this.refs.foodservicePlace.getData()));
        const { categoryId, foodservicePlaceId } = this.props.params;
        this.context.router.push(foodservice(i, foodservicePlaceId || 'new', categoryId));
    }

    handleFoodserviceDelete(i) {
        this.props.dispatch(deleteFoodservice(i));
    }

    handleFoodserviceAdd() {
        this.props.dispatch(persist(this.refs.foodservicePlace.getData()));
        const { categoryId, foodservicePlaceId } = this.props.params;
        this.context.router.push(newFoodservice(foodservicePlaceId || 'new', categoryId));
    }

    handleFoodserviceMove(data) {
        this.props.dispatch(moveFoodservice(data.drag, data.hover));
    }

    handleSave(data) {
        const id = data.get('id');
        const { dispatch } = this.props;

        if (id) {
            dispatch(updateFoodservicePlace(data.toJS()));
        }
        else {
            dispatch(createFoodservicePlace(data.toJS()));
        }
    }
}

export default connect(state => {
    return {
        foodservicePlace: state.get('foodservicePlace')
    }
})(SmartFoodservicePlace);
