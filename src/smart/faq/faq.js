import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import {
    setTitleAsCategory,
    setPrevPath,
    showMessage
} from 'actions/main-layout-action-creators';
import {
    getFaq,
    createFaq,
    updateFaq,
    resetFaq
} from 'actions/faq-action-creators';
import { entities } from 'nav';
import FAQ from 'dumb/faq';
import FallMotion from 'dumb/fall-motion';

const { object, func } = PropTypes;

class SmartFAQ extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        dispatch: func,
        params: object,
        faq: object
    };

    componentWillMount() {
        const { dispatch, params } = this.props;

        dispatch(setTitleAsCategory(params.categoryId));
        dispatch(setPrevPath(entities(params.categoryId)));
        dispatch(getFaq(params.faqId));
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.faq !== this.props.faq;
    }

    componentWillReceiveProps(nextProps) {
        const error = nextProps.faq.get('error')
        if (error) {
            this.props.dispatch(showMessage(error));
        }
        if (nextProps.faq.get('sended')) {
            this.context.router.push(entities(this.props.params.categoryId));
            this.props.dispatch(showMessage('FAQ успешно сохранен'));
        }
    }

    componentWillUnmount() {
        this.props.dispatch(resetFaq());
    }

    render() {
        const faq = this.props.faq;
        const data = faq.get('data');

        if (faq.get('fetching') || data === null) {
            return null;
        }

        return <FallMotion>
            <FAQ
                data={data}
                onSave={data => this.handleSave(data)}
            />
        </FallMotion>;
    }

    handleSave(data) {
        const id = data.get('id');
        const { dispatch } = this.props;

        if (id) {
            dispatch(updateFaq(data.toJS()));
        }
        else {
            dispatch(createFaq(data.toJS()));
        }
    }
}

export default connect(state => {
    return {
        faq: state.get('faq')
    }
})(SmartFAQ);
