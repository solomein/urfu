import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { getCategoryList } from 'actions/category-list-action-creators';
import { setTitle, setPrevPath } from 'actions/main-layout-action-creators';
import CategoryList from 'dumb/category-list';
import { entities } from 'nav'

const { func, object } = PropTypes;

class Categories extends Component {
    static contextTypes = {
        router: object
    };

    static propTypes = {
        dispatch: func,
        categoryList: object
    };

    componentWillMount() {
        this.props.dispatch(setTitle('Категории'));
        this.props.dispatch(setPrevPath(null));
        this.props.dispatch(getCategoryList());
    }

    shouldComponentUpdate(nextProps) {
        return nextProps.categoryList !== this.props.categoryList;
    }

    render() {
        return (
            <section>
                <CategoryList
                    animate={this.props.categoryList.get('animate')}
                    data={this.props.categoryList.get('items')}
                    onSelect={this.handleSelectCategory.bind(this)}
                />
            </section>
        );
    }

    handleSelectCategory(data) {
        this.context.router.push(entities(data.id));
    }
}

export default connect(state => {
    return {
        categoryList: state.get('categoryList')
    }
})(Categories);
