// import { combineReducers } from 'redux';
import { combineReducers } from 'redux-immutablejs';

import mainLayoutReducer from './main-layout-reducer';
import categoryListReducer from './category-list-reducer';
import entityListReducer from './entity-list-reducer';
import placeReducer from './place-reducer';
import faqReducer from './faq-reducer';
import checkListReducer from './check-list-reducer';
import foodservicePlaceReducer from './foodservice-place-reducer';
import campusBuildingReducer from './campus-building-reducer';

const rootReducer = combineReducers({
    mainLayout: mainLayoutReducer,
    categoryList: categoryListReducer,
    entityList: entityListReducer,
    place: placeReducer,
    faq: faqReducer,
    checkList: checkListReducer,
    foodservicePlace: foodservicePlaceReducer,
    campusBuilding: campusBuildingReducer
});

export default rootReducer;
