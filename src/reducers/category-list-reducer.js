import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    error: null,
    fetching: false,
    items: [],
    animate: true
});

export default createReducer(initialState, {
    [Types.GET_CATEGORY_LIST_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true
    }),

    [Types.GET_CATEGORY_LIST_SUCCESS]: (state, { response }) => {
        return state.merge({
            error: null,
            fetching: false,
            items: response,
            animate: state.get('items').size !== response.length
        }
    )},

    [Types.GET_CATEGORY_LIST_FAILURE]: (state, { error }) => state.merge({
        error,
        fetching: false
    })
});
