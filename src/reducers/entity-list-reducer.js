import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    error: null,
    fetching: false,
    items: null,
    raw: [],
    readonly: false,
    maxItems: null
});

export default createReducer(initialState, {
    [Types.GET_ENTITY_LIST_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true
    }),

    [Types.GET_ENTITY_LIST_SUCCESS]: (state, { response, raw, readonly, maxItems }) => state.merge({
        maxItems,
        readonly,
        raw,
        error: null,
        fetching: false,
        items: response
    }),

    [Types.GET_ENTITY_LIST_FAILURE]: (state) => state.merge({
        error: 'Ошибка загрузки списка',
        fetching: false
    }),

    [Types.RESET_ENTITY_LIST]: (state) => state.merge({
        items: null
    }),

    [Types.DELETE_ENTITY_LIST_ITEM_REQUEST]: (state, { id }) => state.merge({
        error: null,
        fetching: true,
        items: state.get('items').map(item => {
            return item.get('id') === id
                ? item.set('disabled', true)
                : item;
        })
    }),

    [Types.DELETE_ENTITY_LIST_ITEM_SUCCESS]: (state, { id }) => state.merge({
        error: null,
        fetching: false,
        items: state.get('items').filter(item => item.get('id') !== id)
    }),

    [Types.DELETE_ENTITY_LIST_ITEM_ERROR]: (state) => state.merge({
        error: 'Ошибка удаления элемента',
        fetching: false
    }),

    [Types.MOVE_ENTITY_LIST_ITEM]: (state, { drag, hover }) => {
        const list = state.get('items');
        const item = list.get(drag);

        return state.merge({
            items: list.splice(drag, 1).splice(hover, 0, item)
        });
    }
});
