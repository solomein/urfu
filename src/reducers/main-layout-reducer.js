import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    title: 'УрФУ',
    prevPath: null,
    messages: []
});

export default createReducer(initialState, {
    [Types.SET_PREV_PATH]: (state, { path }) => state.merge({
        prevPath: path
    }),

    [Types.SET_TITLE]: (state, { title }) => state.merge({
        title
    }),

    [Types.SHOW_MESSAGE]: (state, { message }) => state.merge({
        messages: state.get('messages').push(message)
    }),

    [Types.REMOVE_MESSAGE]: (state, { stamp }) => state.merge({
        messages: state.get('messages').filter(m => m.stamp !== stamp)
    })
});
