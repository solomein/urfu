import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    error: null,
    fetching: false,
    data: null,
    task: {},
    persist: false,
    sended: false
});

export default createReducer(initialState, {
    [Types.GET_CHECK_LIST_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true,
        sended: false
    }),

    [Types.GET_CHECK_LIST_SUCCESS]: (state, { response }) => state.merge({
        error: null,
        fetching: false,
        task: {},
        data: response,
        sended: false
    }),

    [Types.GET_CHECK_LIST_FAILURE]: (state) => state.merge({
        error: 'Ошибка загрузки инструкции',
        fetching: false,
        sended: false
    }),

    [Types.RESET_CHECK_LIST]: (state) => {
        if (state.get('persist')) {
            return state;
        }

        return state.merge({
            data: null
        });
    },

    [Types.SEND_CHECK_LIST_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true,
        sended: false
    }),

    [Types.SEND_CHECK_LIST_SUCCESS]: (state) => state.merge({
        error: null,
        fetching: false,
        sended: true
    }),

    [Types.SEND_CHECK_LIST_FAILURE]: (state) => state.merge({
        error: 'Ошибка сохранения инструкции',
        fetching: false,
        sended: false
    }),

    [Types.PERSIST_CHECK_LIST]: (state, { checkList }) => state.merge({
        data: checkList,
        persist: true
    }),

    [Types.HALT_CHECK_LIST]: (state) => state.merge({
        persist: false
    }),

    [Types.SELECT_TASK]: (state, { index }) => {
        const tasks = state.getIn(['data', 'tasks']);
        const task = tasks
            ? tasks.get(index)
            : state.get('persist')
                ? {}
                : null;

        return state.merge({ task });
    },

    [Types.UPDATE_TASK]: (state, { task, index }) => state.setIn(
        ['data', 'tasks', index], task
    ),

    [Types.ADD_TASK]: (state, { task }) => state.updateIn(
        ['data', 'tasks'], list => list.push(task)
    ),

    [Types.DELETE_TASK]: (state, { index }) => state.updateIn(
        ['data', 'tasks'], list => list.delete(index)
    ),

    [Types.MOVE_TASK]: (state, { drag, hover }) => {
        const list = state.getIn(['data', 'tasks']);
        const item = list.get(drag);

        return state.mergeIn(['data'], {
            tasks: list.splice(drag, 1).splice(hover, 0, item)
        });
    }
});
