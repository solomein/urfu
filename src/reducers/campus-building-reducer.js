import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    error: null,
    fetching: false,
    data: null,
    sended: false
});

export default createReducer(initialState, {
    [Types.GET_CAMPUS_BUILDING_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true,
        sended: false
    }),

    [Types.GET_CAMPUS_BUILDING_SUCCESS]: (state, { response }) => state.merge({
        error: null,
        fetching: false,
        data: response,
        sended: false
    }),

    [Types.GET_CAMPUS_BUILDING_FAILURE]: (state) => state.merge({
        error: 'Ошибка загрузки здания',
        fetching: false,
        sended: false
    }),

    [Types.RESET_CAMPUS_BUILDING]: (state) => state.merge({
        data: null
    }),

    [Types.SEND_CAMPUS_BUILDING_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true,
        sended: false
    }),

    [Types.SEND_CAMPUS_BUILDING_SUCCESS]: (state) => state.merge({
        error: null,
        fetching: false,
        sended: true
    }),

    [Types.SEND_CAMPUS_BUILDING_FAILURE]: (state) => state.merge({
        error: 'Ошибка сохранения здания',
        fetching: false,
        sended: false
    })
});
