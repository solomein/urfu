import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    error: null,
    fetching: false,
    data: null,
    sended: false
});

export default createReducer(initialState, {
    [Types.GET_FAQ_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true,
        sended: false
    }),

    [Types.GET_FAQ_SUCCESS]: (state, { response }) => state.merge({
        error: null,
        fetching: false,
        data: response,
        sended: false
    }),

    [Types.GET_FAQ_FAILURE]: (state) => state.merge({
        error: 'Ошибка загрузки FAQ',
        fetching: false,
        sended: false
    }),

    [Types.RESET_FAQ]: (state) => state.merge({
        data: null
    }),

    [Types.SEND_FAQ_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true,
        sended: false
    }),

    [Types.SEND_FAQ_SUCCESS]: (state) => state.merge({
        error: null,
        fetching: false,
        sended: true
    }),

    [Types.SEND_FAQ_FAILURE]: (state) => state.merge({
        error: 'Ошибка сохранения FAQ',
        fetching: false,
        sended: false
    })
});
