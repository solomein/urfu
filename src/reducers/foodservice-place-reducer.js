import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    error: null,
    fetching: false,
    data: null,
    foodservice: {},
    persist: false,
    sended: false
});

export default createReducer(initialState, {
    [Types.GET_FOODSERVICE_PLACE_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true,
        sended: false
    }),

    [Types.GET_FOODSERVICE_PLACE_SUCCESS]: (state, { response }) => state.merge({
        error: null,
        fetching: false,
        foodservice: {},
        data: response,
        sended: false
    }),

    [Types.GET_FOODSERVICE_PLACE_FAILURE]: (state) => state.merge({
        error: 'Ошибка загрузки места',
        fetching: false,
        sended: false
    }),

    [Types.RESET_FOODSERVICE_PLACE]: (state) => {
        if (state.get('persist')) {
            return state;
        }

        return state.merge({
            data: null
        });
    },

    [Types.SEND_FOODSERVICE_PLACE_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true,
        sended: false
    }),

    [Types.SEND_FOODSERVICE_PLACE_SUCCESS]: (state) => state.merge({
        error: null,
        fetching: false,
        sended: true
    }),

    [Types.SEND_FOODSERVICE_PLACE_FAILURE]: (state) => state.merge({
        error: 'Ошибка сохранения места',
        fetching: false,
        sended: false
    }),

    [Types.PERSIST_FOODSERVICE_PLACE]: (state, { foodservicePlace }) => state.merge({
        data: foodservicePlace,
        persist: true
    }),

    [Types.HALT_FOODSERVICE_PLACE]: (state) => state.merge({
        persist: false
    }),

    [Types.SELECT_FOODSERVICE]: (state, { index }) => {
        const foodservices = state.getIn(['data', 'foodservices']);
        const foodservice = foodservices
            ? foodservices.get(index)
            : state.get('persist')
                ? {}
                : null;

        return state.merge({ foodservice });
    },

    [Types.UPDATE_FOODSERVICE]: (state, { foodservice, index }) => state.setIn(
        ['data', 'foodservices', index], foodservice
    ),

    [Types.ADD_FOODSERVICE]: (state, { foodservice }) => state.updateIn(
        ['data', 'foodservices'], list => list.push(foodservice)
    ),

    [Types.DELETE_FOODSERVICE]: (state, { index }) => state.updateIn(
        ['data', 'foodservices'], list => list.delete(index)
    ),

    [Types.MOVE_FOODSERVICE]: (state, { drag, hover }) => {
        const list = state.getIn(['data', 'foodservices']);
        const item = list.get(drag);

        return state.mergeIn(['data'], {
            foodservices: list.splice(drag, 1).splice(hover, 0, item)
        });
    }
});
