import * as Types from 'actions/action-types';
import { createReducer } from 'redux-immutablejs';
import { fromJS } from 'immutable';

const initialState = fromJS({
    error: null,
    fetching: false,
    data: null,
    sended: false,
    canEditColor: false,
    canEditGradient: false
});

export default createReducer(initialState, {
    [Types.GET_PLACE_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true,
        sended: false
    }),

    [Types.GET_PLACE_SUCCESS]: (state, data) => state.merge({
        error: null,
        fetching: false,
        data: data.response,
        canEditColor: data.canEditColor,
        canEditGradient: data.canEditGradient,
        sended: false
    }),

    [Types.GET_PLACE_FAILURE]: (state) => state.merge({
        error: 'Ошибка загрузки места',
        fetching: false,
        sended: false
    }),

    [Types.SEND_PLACE_REQUEST]: (state) => state.merge({
        error: null,
        fetching: true,
        sended: false
    }),

    [Types.SEND_PLACE_SUCCESS]: (state) => state.merge({
        error: null,
        fetching: false,
        sended: true
    }),

    [Types.SEND_PLACE_FAILURE]: (state) => state.merge({
        error: 'Ошибка сохранения места',
        fetching: false,
        sended: false
    }),

    [Types.RESET_PLACE]: (state) => state.merge({
        data: null
    })
});
