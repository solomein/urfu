import Cookie from 'js-cookie';
import { auth } from 'provider';

const SID_KEY = 'SID';
let sidChecked = false;

export function loggedIn() {
    if (process.env.APP_SKIP_AUTH) {
        return true;
    }

    const result = Boolean(Cookie.get(SID_KEY));

    if (result && !sidChecked) {
        sidChecked = true;
        auth.check();
    }

    return result;
}

export function login(user, password) {
    if (process.env.APP_FAKES) {
        Cookie.set(SID_KEY, Math.random());
    }
    return auth.login(user, password);
}

export function logout() {
    return auth.logout().then(() => {
        Cookie.remove(SID_KEY);
    });
}

export function clear() {
    Cookie.remove(SID_KEY);
}
