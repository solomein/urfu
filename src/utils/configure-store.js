import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import rootReducer from 'reducers/root-reducer';
import { fromJS } from 'immutable';

const middleware = [thunk];

if (process.env.NODE_ENV === 'development') {
    const createLogger = require('redux-logger');
    const logger = createLogger({ collapsed: true });
    middleware.push(logger);
}

const state = fromJS({});
const store = rootReducer(state);

const createStoreWithMiddleware = applyMiddleware(...middleware)(createStore);

export default function configureStore() {
    return createStoreWithMiddleware(rootReducer, store);
}
