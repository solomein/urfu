import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEvent from 'react-tap-event-plugin';
import { Provider } from 'react-redux';
import configureStore from 'utils/configure-store';
import { setBaseUrl } from 'utils/ajax';
import 'styles/index.css';
import Routing from './routing';

setBaseUrl(process.env.APP_API);
injectTapEvent();
const store = configureStore();

ReactDOM.render((
    <Provider store={store}>{Routing}</Provider>
), document.querySelector('main'));
