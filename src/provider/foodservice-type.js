export const CANTEEN = 'Canteen';
export const BUFFET = 'Buffet';

export function getFoodserviceTypes() {
    return [
        {
            value: CANTEEN,
            text: 'Столовая'
        },
        {
            value: BUFFET,
            text: 'Буфет'
        }
    ];
}
