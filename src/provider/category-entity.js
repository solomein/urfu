import * as Type from 'provider/category-type';

export function createCategoryEntityProvider({
    category,
    place,
    faq,
    foodservicePlace,
    campusBuilding,
    checkList
}) {
    //HACK
    function concatSubCategories(category) {
        const req = category.subCategories.map((sub) => {
            return place.getSubCategoryEntityList(category.id, sub.id)
        });

        return Promise.all(req).then(values => {
            return values.reduce((result, value) => {
                return result.concat(value);
            }, []);
        });
    }

    function getEntityProvider(type) {
        if (type === Type.CHECK_LIST) {
            return checkList;
        }

        if (type === Type.FAQ) {
            return faq;
        }

        if (type === Type.CANTEENS) {
            return foodservicePlace;
        }

        if (type === Type.CAMPUS_MAP) {
            return campusBuilding;
        }

        return place;
    }

    function getType(categoryId) {
        return category.getCategory(categoryId).then(category => {
            return category.type;
        });
    }

    return {
        isReadOnlyEntityList(categoryId) {
            return getType(categoryId).then(Type.isReadOnly);
        },

        maxItems(categoryId) {
            return category.getCategory(categoryId).then(category => {
                return category.maxItems;
            });
        },

        getColorEditConfig(categoryId) {
            return getType(categoryId).then(type => {
                const canEditColor = type === Type.CHECK_LIST
                    || type === Type.LIBRARIES;
                const canEditGradient = type === Type.LIBRARIES
                    || type === Type.INSTITUTES;

                return { canEditColor, canEditGradient };
            });
        },

        getEntityList(categoryId) {
            return category.getCategory(categoryId).then(category => {
                if (category.subCategories && category.subCategories.length) {
                    return concatSubCategories(category);
                }

                return getEntityProvider(category.type)
                    .getEntityList(categoryId);
            });
        },

        deleteEntity(entityId, categoryId) {
            return getType(categoryId).then(type => {
                return getEntityProvider(type).deleteEntity(entityId);
            });
        },

        updateEntity(data, categoryId) {
            return getType(categoryId).then(type => {
                return getEntityProvider(type).updateEntity(data);
            });
        },

        convertToListItems(categoryId, data) {
            return getType(categoryId).then(type => {
                return getEntityProvider(type).convertToListItems(data);
            });
        }
    }
}
