export const FAQ = 'FAQ';
export const CHECK_LIST = 'CheckList';
export const INSTITUTES = 'Institutes';
export const DORMITORIES = 'Dormitories';
export const ADMISSION_CENTER = 'AdmissionCenter';
export const VISA_SUPPORT = 'VisaSupport';
export const MEDICIAL_INSURANCE_AND_SERVICE = 'MedicalInsuranceAndService';
export const LIBRARIES = 'Libraries';
export const SPORT = 'Sport';
export const CANTEENS = 'Canteens';
export const NEWS_AND_EVENTS = 'NewsAndEvents';
export const BANKS = 'Banks';
export const CAMPUS_MAP = 'CampusMap';
export const STUDENTS_SUPPORT = 'StudentSupport';

export function getAppPages() {
    return [
        {
            value: '',
            text: 'Нет'
        },
        {
            value: FAQ,
            text: 'F.A.Q.'
        },
        {
            value: CHECK_LIST,
            text: 'Инструкции'
        },
        {
            value: INSTITUTES,
            text: 'Институты'
        },
        {
            value: DORMITORIES,
            text: 'Общежития'
        },
        {
            value: ADMISSION_CENTER,
            text: 'Центр приёма'
        },
        {
            value: VISA_SUPPORT,
            text: 'Визовая поддержка'
        },
        {
            value: MEDICIAL_INSURANCE_AND_SERVICE,
            text: 'Страховка и медицинские услуги'
        },
        {
            value: LIBRARIES,
            text: 'Библиотеки'
        },
        {
            value: SPORT,
            text: 'Спорт'
        },
        {
            value: CANTEENS,
            text: 'Питание'
        },
        {
            value: NEWS_AND_EVENTS,
            text: 'Новости и События'
        },
        {
            value: BANKS,
            text: 'Банк'
        },
        {
            value: CAMPUS_MAP,
            text: 'Карта кампуса'
        },
        {
            value: STUDENTS_SUPPORT,
            text: 'Помощь студенту'
        }
    ];
}
