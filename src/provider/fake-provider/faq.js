import fakeFaqs from 'provider/fake-data/fake-faqs';
import mockPost from './mock-post';

export default {
    getEntityList() {
        return new Promise((resolve) => {
            resolve(fakeFaqs);
        });
    },
    getEntity() {
        return new Promise((resolve) => {
            resolve(fakeFaqs[0]);
        });
    },
    createEntity: mockPost('create faq'),
    updateEntity: mockPost('update faq'),
    deleteEntity: mockPost('delete faq'),
    convertToListItems(data) {
        return data.map(entity => {
            return {
                id: entity.id,
                primaryText: entity.question.russian
            };
        });
    }
}
