import fakeFoodservices from 'provider/fake-data/fake-foodservices';
import mockPost from './mock-post';

export default {
    getEntityList() {
        return new Promise((resolve) => {
            resolve(fakeFoodservices);
        });
    },
    getEntity() {
        return new Promise((resolve) => {
            resolve(fakeFoodservices[0]);
        });
    },
    createEntity: mockPost('create foodservice place'),
    updateEntity: mockPost('update foodservice place'),
    deleteEntity: mockPost('delete foodservice place'),
    convertToListItems(data) {
        return data.map(entity => {
            return {
                id: entity.id,
                primaryText: entity.address.title.russian
            };
        });
    }
}
