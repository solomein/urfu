import fakeCheckLists from 'provider/fake-data/fake-check-lists';
import mockPost from './mock-post';

export default {
    getEntityList() {
        return new Promise((resolve) => {
            resolve(fakeCheckLists);
        });
    },
    getEntity() {
        return new Promise((resolve) => {
            resolve(fakeCheckLists[0]);
        });
    },
    createEntity: mockPost('create check list'),
    updateEntity: mockPost('update check list'),
    deleteEntity: mockPost('delete check list'),
    convertToListItems(data) {
        return data.map(entity => {
            return {
                id: entity.id,
                primaryText: entity.title.russian,
                secondaryText: entity.subTitle.russian
            };
        });
    }
}
