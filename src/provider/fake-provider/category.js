import fakeCategories from 'provider/fake-data/fake-categories';

export default {
    getCategoryList() {
        return new Promise((resolve) => {
            resolve(fakeCategories);
        });
    }
}
