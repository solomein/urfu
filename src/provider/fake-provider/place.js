import fakePlaces from 'provider/fake-data/fake-places';
import mockPost from './mock-post';

export default {
    getEntityList() {
        return new Promise((resolve) => {
            resolve(fakePlaces);
        });
    },
    getSubCategoryEntityList() {
        return new Promise((resolve) => {
            resolve(fakePlaces);
        });
    },
    getEntity() {
        return new Promise((resolve) => {
            resolve(fakePlaces[0]);
        });
    },
    createEntity: mockPost('create place'),
    updateEntity: mockPost('update place'),
    deleteEntity: mockPost('delete place'),
    convertToListItems(data) {
        return data.map(entity => {
            return {
                id: entity.id,
                primaryText: entity.title.russian,
                secondaryText: entity.subTitle.russian
            };
        });
    },
    uploadImage() {
        return new Promise(resolve => {
            resolve(Math.random().toString());
        });
    },

    downloadImage() {}
}
