/*eslint no-console:0*/

export default function mockPost(msg) {
    return function(data) {
        console.log(msg, data);
        return new Promise(resolve => {
            resolve(data);
        });
    }
}
