import fakeCampusBuildings from 'provider/fake-data/fake-campus-buildings';
import mockPost from './mock-post';

export default {
    getEntityList() {
        return new Promise((resolve) => {
            resolve(fakeCampusBuildings);
        });
    },
    getEntity() {
        return new Promise((resolve) => {
            resolve(fakeCampusBuildings[0]);
        });
    },
    createEntity: mockPost('create campus building'),
    updateEntity: mockPost('update campus building'),
    deleteEntity: mockPost('delete campus building'),
    convertToListItems(data) {
        return data.map(entity => {
            return {
                id: entity.id,
                primaryText: entity.title.russian
            };
        });
    }
}
