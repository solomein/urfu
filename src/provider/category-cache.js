let categoryListPromise = null;

const categoryListCache = {
    cache: [],
    index: {},

    set(data) {
        this.cache.length = 0;
        this.cache = data;
        this.index = {};
        this.cache.forEach((category, i) => this.index[category.id] = i);
    },

    get() {
        return this.cache;
    },

    getById(id) {
        return this.cache[this.index[id]];
    },

    isFilled() {
        return this.cache.length > 0;
    }
};

export function createCategoryCacheProvider({ category }) {
    function getCategoryType(categoryId) {
        return getCategory(categoryId).then(category => {
            return category.type;
        });
    }

    function getCategoryList() {
        if (categoryListPromise) {
            return categoryListPromise;
        }

        return categoryListPromise = category.getCategoryList()
            .then(data => {
                categoryListCache.set(data);
                return data;
            });
    }

    function getCategory(categoryId) {
        return new Promise((resolve) => {
            if (categoryListCache.isFilled()) {
                resolve(categoryListCache.getById(categoryId));
            }
            else {
                getCategoryList().then(() => {
                    resolve(categoryListCache.getById(categoryId));
                });
            }
        });
    }

    return { getCategoryType, getCategoryList, getCategory }
}
