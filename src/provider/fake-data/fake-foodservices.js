const data = [
    {
        "id": "string",
        "address": {
            "title": {
                "russian": "string",
                "english": "string"
            },
            "details": {
                "russian": "string",
                "english": "string"
            },
            "coordinates": {
                "latitude": 56.831477702653075,
                "longitude": 60.53892698364106
            }
        },
        "foodservices": [
            {
                "id": "string",
                "title": {
                    "russian": "string",
                    "english": "string"
                },
                "type": 'Canteen',
                "workTimes": [
                    {
                        "startTime": "string",
                        "endTime": "string",
                        "description": {
                            "russian": "string",
                            "english": "string"
                        },
                        "weekDays": [
                            "Monday"
                        ]
                    }
                ]
            }
        ]
    }
];

export default data;
