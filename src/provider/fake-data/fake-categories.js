import * as CategoryType from 'provider/category-type';

const data = [
    {
        id: '0',
        title: {
            russian: 'Инструкции'
        },
        type: CategoryType.CHECK_LIST
    },
    {
        id: '1',
        title: {
            russian: 'Институты'
        },
        type: CategoryType.INSTITUTES,
        maxItems: 2
    },
    {
        id: '2',
        title: {
            russian: 'Общежития'
        },
        type: CategoryType.DORMITORIES
    },
    {
        id: '3',
        title: {
            russian: 'F.A.Q.'
        },
        type: CategoryType.FAQ
    },
    {
        id: '4',
        title: {
            russian: 'Питание'
        },
        type: CategoryType.CANTEENS
    },
    {
        id: '5',
        title: {
            russian: 'Карта кампуса'
        },
        type: CategoryType.CAMPUS_MAP
    },
    {
        id: '6',
        title: {
            russian: 'Спорт'
        },
        type: CategoryType.SPORT,
        subCategories: [
            {
                id: '6.1'
            },
            {
                id: '6.2'
            }
        ]
    }
];

export default data;
