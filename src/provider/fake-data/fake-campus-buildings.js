const data = [
    {
        "id": "string",
        "title": {
            "russian": "string",
            "english": "string"
        },
        "floors": [
            {
                "id": "string1",
                "number": 3,
                "map": {
                    "id": "string11",
                    "name": "string string string string string"
                }
            },
            {
                "id": "string2",
                "number": 1,
                "map": {
                    "id": "string12",
                    "name": "string"
                }
            }
        ],
        "color": "string"
    }
];

export default data;
