const data = [
    {
        "id": "string",
        "title": {
            "russian": "string",
            "english": "string"
        },
        "subTitle": {
            "russian": "string",
            "english": "string"
        },
        "tasks": [
            {
                "additionalText": {
                    "title": {
                        "russian": "string",
                        "english": "string"
                    },
                    "text": {
                        "russian": "string",
                        "english": "string"
                    }
                },
                "contacts": {
                    "title": {
                        "russian": "string",
                        "english": "string"
                    },
                    "addresses": [
                        {
                            "title": {
                                "russian": "string",
                                "english": "string"
                            },
                            "details": {
                                "russian": "string",
                                "english": "string"
                            },
                            "coordinates": {
                                "latitude": 0,
                                "longitude": 0
                            }
                        }
                    ]
                },
                "pageLink": {
                    "title": {
                        "russian": "string",
                        "english": "string"
                    },
                    "page": "FAQ"
                },
                "externalLink": {
                    "title": {
                        "russian": "string",
                        "english": "string"
                    },
                    "url": "string"
                },
                "telephone": {
                    "title": {
                        "russian": "string",
                        "english": "string"
                    },
                    "number": "string"
                },
                "id": "string11",
                "title": {
                    "russian": "string11",
                    "english": "string1"
                },
                "description": {
                    "russian": "string",
                    "english": "string"
                }
            },
            {
                "additionalText": {
                    "title": {
                        "russian": "string",
                        "english": "string"
                    },
                    "text": {
                        "russian": "string",
                        "english": "string"
                    }
                },
                "contacts": {
                    "title": {
                        "russian": "string",
                        "english": "string"
                    },
                    "addresses": [
                        {
                            "title": {
                                "russian": "string",
                                "english": "string"
                            },
                            "details": {
                                "russian": "string",
                                "english": "string"
                            },
                            "coordinates": {
                                "latitude": 0,
                                "longitude": 0
                            }
                        }
                    ]
                },
                "pageLink": {
                    "title": {
                        "russian": "string",
                        "english": "string"
                    },
                    "page": "FAQ"
                },
                "externalLink": {
                    "title": {
                        "russian": "string",
                        "english": "string"
                    },
                    "url": "string"
                },
                "telephone": {
                    "title": {
                        "russian": "string",
                        "english": "string"
                    },
                    "number": "string"
                },
                "id": "string",
                "title": {
                    "russian": "string",
                    "english": "string"
                },
                "description": {
                    "russian": "string",
                    "english": "string"
                }
            }
        ],
        "color": "string",
        "iconName": "string",
        "orderNumber": 0
    }
];

export default data;
