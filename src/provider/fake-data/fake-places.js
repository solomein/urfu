const testData = [
    {
        id: '114',
        categoryId: '1',
        title: {
            russian: 'Строительный институт',
            english: 'Institute of Civil Engineering'
        },
        subTitle: {
            russian: 'Мира 17',
            english: '17 Mira st.'
        },
        categoryType: '2',
        workTimes: [
            {
                startTime: '00:11:00.1234567',
                endTime: '00:00:00.1234567',
                breakTime: '00:00:00.1234567',
                weekDays: [
                    0,
                    0
                ]
            },
            {
                startTime: '12:05:00.1234567',
                endTime: '00:00:00.1234567',
                breakTime: '00:00:00.1234567',
                weekDays: [
                    0,
                    0
                ]
            }
        ],
        address: {

        },
        telephones: ['+ 7(343) 374-59-82'],
        color: '#3A9DC6',
        images: [
            {
                id: '11421',
                fileName: 'image.png',
                description: 'description'
            }
        ]
    },
    {
        id: '1141',
        categoryId: '1',
        title: {
            russian: 'Строительный институт 2',
            english: 'Institute of Civil Engineering'
        },
        subTitle: {
            russian: 'Мира 17',
            english: '17 Mira st.'
        },
        categoryType: '2',
        workTimes: [
            {
                startTime: '00:00:00.1234567',
                endTime: '00:00:00.1234567',
                breakTime: '00:00:00.1234567',
                weekDays: [
                    0,
                    0
                ]
            },
            {
                startTime: '00:00:00.1234567',
                endTime: '00:00:00.1234567',
                breakTime: '00:00:00.1234567',
                weekDays: [
                    0,
                    0
                ]
            }
        ],
        address: {

        },
        telephones: ['+ 7(343) 374-59-82'],
        color: '#3A9DC6',
        images: []
    },
    {
        id: '1142',
        categoryId: '1',
        title: {
            russian: 'Строительный институт 3',
            english: 'Institute of Civil Engineering'
        },
        subTitle: {
            russian: 'Мира 17',
            english: '17 Mira st.'
        },
        categoryType: '2',
        workTimes: [
            {
                startTime: '00:00:00.1234567',
                endTime: '00:00:00.1234567',
                breakTime: '00:00:00.1234567',
                weekDays: [
                    0,
                    0
                ]
            },
            {
                startTime: '00:00:00.1234567',
                endTime: '00:00:00.1234567',
                breakTime: '00:00:00.1234567',
                weekDays: [
                    0,
                    0
                ]
            }
        ],
        address: {

        },
        telephones: ['+ 7(343) 374-59-82'],
        color: '#3A9DC6',
        images: [
            {
                id: '11421',
                fileName: 'image.png',
                description: 'description'
            }
        ]
    }
];

export default testData;
