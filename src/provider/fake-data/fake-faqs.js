const data = [
    {
        id: '0',
        question: {
            russian: 'Кто-нибудь может встретить меня в аэропорту?',
            english: 'Can anyone meet me in the airport?'
        },
        answer: {
            russian: 'Да! Вам нужно позвонить в центр помощи студентам и сообщить дату и время прибытия. Наши волонтеры с удовольствием встретят Вас.',
            english: 'Yes! You need to call to our help center and report the date and time of your arrival. Our volunteers will be happy to meet you.'
        },
        additionalText: {
            title: {
                russian: 'Список документов'
            },
            text: {
                russian: 'Список'
            }
        },
        telephone: {
            title: {
                russian: 'Позвонить в центр помощи',
                english: 'call to centre'
            },
            number: '+79123462621'
        }
    }
];

export default data;
