export const FAQ = 'FAQ';
export const CHECK_LIST = 'CheckList';
export const INSTITUTES = 'Institutes';
export const DORMITORIES = 'Dormitories';
export const ADMISSION_CENTER = 'AdmissionCenter';
export const VISA_SUPPORT = 'VisaSupport';
export const MEDICIAL_INSURANCE_AND_SERVICE = 'MedicalInsuranceAndService';
export const LIBRARIES = 'Libraries';
export const SPORT = 'Sport';
export const CANTEENS = 'Canteens';
export const NEWS_AND_EVENTS = 'NewsAndEvents';
export const BANKS = 'Banks';
export const CAMPUS_MAP = 'CampusMap';
export const STUDENTS_SUPPORT = 'StudentSupport';

export function isReadOnly(type) {
    return type === NEWS_AND_EVENTS;
}
