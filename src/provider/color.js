export function getColors() {
    return [
        { color: '#feb000' }, // yellow
        { color: '#747474' }, // grey
        { color: '#00a2d3' }, // blue
        { color: '#7fc600' }, // green
        { color: '#e43325' }, // red
        { color: '#e50079' }, // pink
        {}
    ];
}

export function getGradients() {
    return [
        { from: '#cf091a', to: '#ffa361' },
        { from: '#00234d', to: '#00a7e7' },
        { from: '#138237', to: '#bddaad' },
        { from: '#ff7f00', to: '#009540' },
        { from: '#d5dbde', to: '#0057a3' },
        { from: '#004e91', to: '#5e9cbb' },
        { from: '#0056a4', to: '#00acf0' },
        { from: '#a2de5e', to: '#00983e' },
        { from: '#ff1c15', to: '#ffa400' },
        { from: '#fdc907', to: '#82298f' },
        { from: '#ffb600', to: '#ff161f' },
        { from: '#0090a0', to: '#bde4eb' },
        { from: '#632401', to: '#f9a94a' },
        { from: '#ff142b', to: '#005196' },
        { from: '#6a2995', to: '#e64aa7' },

        { from: '#e43325', to: '#e43325' },
        { from: '#00a2d3', to: '#00a2d3' },
        { from: '#feb000', to: '#feb000' },
        { from: '#e50079', to: '#e50079' },

        {}
    ];
}
