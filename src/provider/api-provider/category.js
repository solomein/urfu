import { get } from 'utils/ajax';
import { NEWS_AND_EVENTS } from 'provider/category-type';

export default {
    getCategoryList() {
        return get('getCategories').then(data =>
            data.filter(c => c.type !== NEWS_AND_EVENTS)
        );
    }
}
