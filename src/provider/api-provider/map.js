import { post, getUrl } from 'utils/ajax';

export default {
    uploadMap(formData) {
        return post('uploadCampusBuildingFloorMap', function () {
            return new FormData(formData);
        }, {});
    },

    downloadMap(id) {
        location.href = getUrl('getCampusBuildingFloorMap', { id });
    }
}
