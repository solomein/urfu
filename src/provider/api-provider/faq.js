import { get, post, put, del } from 'utils/ajax';

export default {
    getEntityList() {
        return get('getListOfFAQ');
    },
    getEntity(id) {
        return get('getFaq', { id });
    },
    createEntity(data) {
        return post('createFaq', data);
    },
    updateEntity(data) {
        return put('updateFaq', data);
    },
    deleteEntity(id) {
        return del('deleteFaq', id);
    },
    convertToListItems(data) {
        return data.map(entity => {
            return {
                id: entity.id,
                primaryText: entity.question && entity.question.russian
            };
        });
    }
}
