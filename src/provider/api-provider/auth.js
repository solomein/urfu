import { post, get } from 'utils/ajax';

export default {
    login(user, password) {
        return post('login', {
            login: user,
            password
        });
    },

    logout() {
        return post('logout');
    },

    check() {
        return get('checkSid');
    }
}
