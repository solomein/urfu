import place from './place';
import faq from './faq';
import foodservicePlace from './foodservice-place';
import campusBuilding from './campus-building';
import checkList from './check-list';
import category from './category';
import map from './map';
import auth from './auth';

export {
    place,
    faq,
    foodservicePlace,
    campusBuilding,
    checkList,
    category,
    map,
    auth
};
