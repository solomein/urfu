import { get, post, put, del } from 'utils/ajax';

export default {
    getEntityList() {
        return get('getInstructions');
    },
    getEntity(id) {
        return get('getInstruction', { id });
    },
    createEntity(data) {
        return post('createInstruction', data);
    },
    updateEntity(data) {
        return put('updateInstruction', data);
    },
    deleteEntity(id) {
        return del('deleteInstruction', id);
    },
    convertToListItems(data) {
        return data.map(entity => {
            return {
                id: entity.id,
                primaryText: entity.title && entity.title.russian,
                secondaryText: entity.subTitle && entity.subTitle.russian
            };
        });
    }
}
