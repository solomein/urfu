import { get, post, put, del } from 'utils/ajax';

export default {
    getEntityList() {
        return get('getFoodservicePlaces');
    },
    getEntity(id) {
        return get('getFoodservicePlace', { id });
    },
    createEntity(data) {
        return post('createFoodservicePlace', data);
    },
    updateEntity(data) {
        return put('updateFoodservicePlace', data);
    },
    deleteEntity(id) {
        return del('deleteFoodservicePlace', id);
    },
    convertToListItems(data) {
        return data.map(entity => {
            return {
                id: entity.id,
                primaryText: entity.address
                    && entity.address.title
                    && entity.address.title.russian
            };
        });
    }
}
