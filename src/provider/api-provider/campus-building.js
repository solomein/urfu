import { get, post, put, del } from 'utils/ajax';

export default {
    getEntityList() {
        return get('getCampusBuildings');
    },
    getEntity(id) {
        return get('getCampusBuilding', { id });
    },
    createEntity(data) {
        return post('createCampusBuilding', data);
    },
    updateEntity(data) {
        return put('updateCampusBuilding', data);
    },
    deleteEntity(id) {
        return del('deleteCampusBuilding', id);
    },
    convertToListItems(data) {
        return data.map(entity => {
            return {
                id: entity.id,
                primaryText: entity.title && entity.title.russian
            };
        });
    }
}
