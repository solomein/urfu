import { get, post, put, del, getUrl } from 'utils/ajax';

export default {
    getEntityList(categoryId) {
        return get('getPlaces', {
            categoryId
        });
    },
    getSubCategoryEntityList(categoryId, subCategoryId) {
        return get('getPlaces', {
            categoryId,
            subCategoryId
        });
    },
    getEntity(id) {
        return get('getPlace', { id });
    },
    createEntity(data) {
        return post('createPlace', data);
    },
    updateEntity(data) {
        return put('updatePlace', data);
    },
    deleteEntity(id) {
        return del('deletePlace', id);
    },
    convertToListItems(data) {
        return data.map(entity => {
            return {
                id: entity.id,
                primaryText: entity.title && entity.title.russian,
                secondaryText: entity.subTitle && entity.subTitle.russian
            };
        });
    },
    uploadImage(formData) {
        return post('uploadPlaceImage', function () {
            return new FormData(formData);
        }, {});
    },

    downloadImage(id) {
        location.href = getUrl('getPlaceImage', { id });
    }
}
