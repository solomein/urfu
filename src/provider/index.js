import { createCategoryEntityProvider } from 'provider/category-entity';
import { createCategoryCacheProvider } from 'provider/category-cache';

let provider;

if (process.env.APP_FAKES) {
    provider = require('./fake-provider');
}
else {
    provider = require('./api-provider');
}

export * as CategoryType from './category-type';

export {
    getFoodserviceTypes
} from './foodservice-type';

export {
    getAppPages
} from './app-page';

export {
    getColors,
    getGradients
} from './color';

export const map = provider.map;
export const auth = provider.auth;
export const place = provider.place;
export const foodservicePlace = provider.foodservicePlace;
export const checkList = provider.checkList;
export const campusBuilding = provider.campusBuilding;
export const faq = provider.faq;
export const category = createCategoryCacheProvider({
    category: provider.category
});

export const categoryEntity = createCategoryEntityProvider({
    category,
    place,
    foodservicePlace,
    checkList,
    campusBuilding,
    faq
});
